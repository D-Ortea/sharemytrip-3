package uo.sdi.client.util;

import java.util.List;

import de.vandermeer.asciitable.v2.V2_AsciiTable;
import de.vandermeer.asciitable.v2.render.V2_AsciiTableRenderer;
import de.vandermeer.asciitable.v2.render.WidthAbsoluteEven;
import de.vandermeer.asciitable.v2.themes.V2_E_TableThemes;

import com.sdi.ws.*;



public class Printer {

	public static void listUsers(List<User> users){
		V2_AsciiTable at = new V2_AsciiTable();
		
		at.addStrongRule();
		at.addRow("Login","Nombre","Apellidos","Email","Promovidos","Participante");
		at.addStrongRule();
		
		for (User user : users) {
			listUser(user,at);
		}
		at.addStrongRule();
		printTable(at);
	}
	
	public static void listUser(User user,V2_AsciiTable at){
		UserService service = new EjbUserServiceImplService().getUserServicePort();
		at.addRow(user.getLogin(),
				user.getName(),
				user.getSurname(),
				user.getEmail(),
				service.getViajesPromovidos(user),
				service.getViajesParticipante(user));	
	}
	
	public static void listRating(Rating rating, V2_AsciiTable at) throws Exception_Exception {
		RatingService service = new EjbRatingServiceImplService().getRatingServicePort();
		List<String> datos = service.getDatosRating(rating);
		at.addRow(rating.getId(),
				datos.get(0),
				datos.get(1),
				datos.get(2),
				rating.getValue(),
				rating.getComment());
	}
	
	public static void listComments(List<Rating> comments) throws Exception_Exception{
		V2_AsciiTable at = new V2_AsciiTable();
		
		at.addStrongRule();
		at.addRow("ID","Destino","Autor","Acerca de","Valoracion","Comentario");
		at.addStrongRule();
		
		for (Rating rating : comments) {
			listRating(rating, at);
		}
		at.addStrongRule();
		printTable(at);
	}
	
	private static void printTable(V2_AsciiTable table){
		V2_AsciiTableRenderer rend = new V2_AsciiTableRenderer();
		rend.setTheme(V2_E_TableThemes.UTF_LIGHT.get());
		rend.setWidth(new WidthAbsoluteEven(200));
		System.out.println(rend.render(table));
	}
}
