package uo.sdi.client.action;

import java.util.List;





import com.sdi.ws.EjbUserServiceImplService;
import com.sdi.ws.User;
import com.sdi.ws.UserService;

import uo.sdi.client.util.Action;
import uo.sdi.client.util.Printer;

public class ListarUsuariosAction implements Action{

	@Override
	public void execute() throws Exception {
		UserService service = new EjbUserServiceImplService().getUserServicePort();
		List<User> users = service.listUsers();
		
		Printer.listUsers(users);
	}
}
