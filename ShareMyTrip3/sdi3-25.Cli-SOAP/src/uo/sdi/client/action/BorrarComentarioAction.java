package uo.sdi.client.action;

import com.sdi.ws.EjbRatingServiceImplService;
import com.sdi.ws.RatingService;

import alb.util.console.Console;
import uo.sdi.client.util.Action;
import uo.sdi.client.util.Printer;

public class BorrarComentarioAction implements Action {

	@Override
	public void execute() throws Exception {
		RatingService service = new EjbRatingServiceImplService().getRatingServicePort();

		System.out.println("Aqui se muestra una lista de comentarios recientes");
		Printer.listComments(service.getRecentRatings());
		System.out.println("Escriba el ID del comentario que quiere eliminar\n");		
		Long id = Console.readLong();		
		
		if(service.borrarComentario(id)){
			System.out.format("Comentario [%d] borrado",id);
		} else {
			System.out.format("No se ha podido encontrar "
					+ "el comentario con id [%d]" ,id);
		}
		
	}
}
