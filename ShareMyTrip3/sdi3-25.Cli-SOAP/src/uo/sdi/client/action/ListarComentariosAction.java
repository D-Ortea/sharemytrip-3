package uo.sdi.client.action;

import com.sdi.ws.EjbRatingServiceImplService;
import com.sdi.ws.RatingService;

import uo.sdi.client.util.Action;
import uo.sdi.client.util.Printer;

public class ListarComentariosAction implements Action{

	@Override
	public void execute() throws Exception {
		RatingService service = new EjbRatingServiceImplService().getRatingServicePort();
		
		Printer.listComments(service.getRecentRatings());
		
	}
}
