package uo.sdi.client.menu;

import uo.sdi.client.action.BorrarComentarioAction;
import uo.sdi.client.action.DeactivateUserAction;
import uo.sdi.client.action.ListarComentariosAction;
import uo.sdi.client.action.ListarUsuariosAction;
import uo.sdi.client.util.BaseMenu;

public class MainMenu extends BaseMenu {

    public MainMenu() {
	menuOptions = new Object[][] {
	    { "Share My Trip EJB Client", null },
	    
	    { "Listado de Usuarios",                ListarUsuariosAction.class },
	    { "Deshabilitar un Usuario",            DeactivateUserAction.class },
	    { "Listar comentarios recientes",       ListarComentariosAction.class },
	    { "Eliminar Comentarios",               BorrarComentarioAction.class },
	};
    }

    public static void main(String[] args) {
    	new MainMenu().execute();
    }

}