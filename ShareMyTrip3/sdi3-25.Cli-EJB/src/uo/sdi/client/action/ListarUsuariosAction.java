package uo.sdi.client.action;

import java.util.List;

import uo.sdi.business.UserService;
import uo.sdi.business.impl.remote.RemoteEjbServicesLocator;
import uo.sdi.model.User;

import uo.sdi.client.util.Action;
import uo.sdi.client.util.Printer;

public class ListarUsuariosAction implements Action{

	@Override
	public void execute() throws Exception {
		UserService service = new RemoteEjbServicesLocator().getUserService();
		
		List<User> users = service.listUsers();
		
		Printer.listUsers(users);
	}
}
