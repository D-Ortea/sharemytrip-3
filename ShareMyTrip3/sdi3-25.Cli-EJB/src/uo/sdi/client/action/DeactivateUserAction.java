package uo.sdi.client.action;

import java.util.List;

import uo.sdi.business.UserService;
import uo.sdi.business.impl.remote.RemoteEjbServicesLocator;
import uo.sdi.model.User;
import alb.util.console.Console;

import uo.sdi.client.util.Action;
import uo.sdi.client.util.Printer;

public class DeactivateUserAction implements Action {

	@Override
	public void execute() throws Exception {
		UserService service = new RemoteEjbServicesLocator().getUserService();
		
		List<User> users = service.listActiveUsers();
		
		System.out.println("Aqui se muestra una lista de los usuarios activos del sistema");
		Printer.listUsers(users);
		System.out.println("Escriba el login del usuario que quiere desactivar\n");
		
		String login = Console.readString();
		
		if(service.deactivateUser(login)){
			System.out.format("Usuario [%s] desactivado",login);
		} else {
			System.out.format("No se ha podido encontrar el usuario [%s]",login);
		}
	}

}
