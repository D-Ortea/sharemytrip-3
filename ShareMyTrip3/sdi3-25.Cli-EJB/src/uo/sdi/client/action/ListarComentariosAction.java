package uo.sdi.client.action;


import uo.sdi.business.RatingService;
import uo.sdi.business.impl.remote.RemoteEjbServicesLocator;
import uo.sdi.client.util.Action;
import uo.sdi.client.util.Printer;

public class ListarComentariosAction implements Action{

	@Override
	public void execute() throws Exception {
		RatingService service = new RemoteEjbServicesLocator().getRatingService();
		
		Printer.listComments(service.getRecentRatings());
		
	}
}
