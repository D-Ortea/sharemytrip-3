package uo.sdi.client.util;

import java.util.List;

import de.vandermeer.asciitable.v2.V2_AsciiTable;
import de.vandermeer.asciitable.v2.render.V2_AsciiTableRenderer;
import de.vandermeer.asciitable.v2.render.WidthAbsoluteEven;
import de.vandermeer.asciitable.v2.themes.V2_E_TableThemes;
import uo.sdi.model.Rating;
import uo.sdi.model.User;

public class Printer {

	public static void listUsers(List<User> users){
		V2_AsciiTable at = new V2_AsciiTable();
		
		at.addStrongRule();
		at.addRow("Login","Nombre","Apellidos","Email","Status","Promovidos","Participante");
		at.addStrongRule();
		
		for (User user : users) {
			listUser(user,at);
		}
		at.addStrongRule();
		printTable(at);
	}
	
	public static void listUser(User user,V2_AsciiTable at){
		at.addRow(user.getLogin(),
				user.getName(),
				user.getSurname(),
				user.getEmail(),
				user.getStatus().name(),
				user.getViajesPromovidos(),
				user.getViajesParticipante());	
	}
	
	public static void listRating(Rating rating, V2_AsciiTable at) {
		at.addRow( rating.getId(),
				rating.getAbout().getTrip().getDestination().toString(),
				rating.getFrom().getUser().toString(),
				rating.getAbout().getUser().toString(),
				rating.getValue(),
				rating.getComment());
	}
	
	public static void listComments(List<Rating> comments){		
		V2_AsciiTable at = new V2_AsciiTable();
		
		at.addStrongRule();
		at.addRow("ID","Destino","Autor","Acerca de","Valoracion","Comentario");
		at.addStrongRule();
		
		for (Rating rating : comments) {
			listRating(rating, at);
		}
		at.addStrongRule();
		printTable(at);
	}
	
	private static void printTable(V2_AsciiTable table){
		V2_AsciiTableRenderer rend = new V2_AsciiTableRenderer();
		rend.setTheme(V2_E_TableThemes.UTF_LIGHT.get());
		rend.setWidth(new WidthAbsoluteEven(200));
		System.out.println(rend.render(table));
	}
}
