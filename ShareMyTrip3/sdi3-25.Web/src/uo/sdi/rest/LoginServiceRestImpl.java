package uo.sdi.rest;

import uo.sdi.business.LoginService;
import uo.sdi.infraestructura.Factories;
import uo.sdi.model.User;

public class LoginServiceRestImpl implements LoginServiceRest{

	LoginService service = Factories.services.getLoginService();
	
	@Override
	public User login(String username, String password) throws Exception {
		return service.verify(username, password);
	}

}
