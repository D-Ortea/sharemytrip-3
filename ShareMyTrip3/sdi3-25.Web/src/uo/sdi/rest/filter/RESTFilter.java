package uo.sdi.rest.filter;

import java.io.IOException;

import javax.servlet.DispatcherType;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.DatatypeConverter;

import alb.util.log.Log;
import uo.sdi.infraestructura.Factories;
import uo.sdi.model.User;

/**
 * Servlet Filter implementation class RESTFilter
 */
@WebFilter(dispatcherTypes = { DispatcherType.REQUEST }, description = "filter for rest", urlPatterns = { "/rest/*" })
public class RESTFilter implements Filter {

	FilterConfig config = null;
	
	/**
	 * Default constructor.
	 */
	public RESTFilter() {
	}

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
		// Si no es petición HTTP nada que hacer
		if (!(request instanceof HttpServletRequest)) {
			chain.doFilter(request, response);
			return;
		}
		
		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse res = (HttpServletResponse) response;
		
		String header = req.getHeader("Authorization");
		if(header == null || header.isEmpty()){
			res.sendError(HttpServletResponse.SC_UNAUTHORIZED);
			Log.warn("Unauthorized Rest petition to [%s]", req.getRequestURL());
			return;
		}
		
		String[] credentials = decodeAthorization(header);
		try {
			User user = Factories.services.getLoginService().verify(credentials[0], credentials[1]);
			if(user == null)
				throw new RuntimeException();
		} catch (Exception e) {
			res.sendError(HttpServletResponse.SC_UNAUTHORIZED);
			Log.warn("Unauthorized Rest petition to [%s] from user [%s]",
					req.getRequestURL(),credentials[0]);
			return;
		}

		chain.doFilter(request, response);
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		config = fConfig;
	}

	private String[] decodeAthorization(String auth){
		byte[] res = DatatypeConverter.parseBase64Binary(auth);
		String base64 = new String(res);
		return base64.split(":");
	}
}
