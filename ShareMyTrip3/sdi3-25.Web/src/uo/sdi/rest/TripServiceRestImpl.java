package uo.sdi.rest;

import java.util.List;

import javax.ws.rs.core.Response;

import uo.sdi.business.TripService;
import uo.sdi.infraestructura.Factories;
import uo.sdi.model.Trip;
import uo.sdi.model.User;

public class TripServiceRestImpl implements TripServiceRest{

	TripService service = Factories.services.getTripService();
	
	@Override
	public List<Trip> getPromotedTrips(Long id) throws Exception {
		return service.listActivePromoted(id);
	}

	@Override
	public List<User> getPasajerosSinConfirmar(Long id) throws Exception {
		return service.listPasajerosSinConfirmar(id);
	}

	@Override
	public Response confirmarPasajero(Long tripId, Long userId) throws Exception {
		service.confirmarPasajeroId(tripId, userId);
		return Response.ok().build();
	}

	@Override
	public List<Trip> participacionActiva(Long id) throws Exception {
		return service.viajesConParticipacion(id);
	}

	
}
