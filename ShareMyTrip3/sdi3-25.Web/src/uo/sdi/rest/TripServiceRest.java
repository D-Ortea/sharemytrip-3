package uo.sdi.rest;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import uo.sdi.model.Trip;
import uo.sdi.model.User;

@Path("/")
public interface TripServiceRest {
	@GET
	@Path("/promotor/{id}")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	List<Trip> getPromotedTrips(@PathParam("id") Long id) throws Exception;

	@GET
	@Path("/pasajeros/{id}")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	List<User> getPasajerosSinConfirmar(@PathParam("id") Long id)
			throws Exception;

	@POST
	@Path("/promotor/confirmar")
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	Response confirmarPasajero(@QueryParam("trip") Long tripId,
			@QueryParam("user") Long userId) throws Exception;
	
	@GET
	@Path("/viajes/{id}")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	List<Trip> participacionActiva(@PathParam("id") Long id) throws Exception;
}
