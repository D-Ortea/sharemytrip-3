package uo.sdi.rest;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import uo.sdi.model.User;

@Path("/")
public interface LoginServiceRest {

	@GET
	@Path("/login")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	User login(@QueryParam("username") String username,
			@QueryParam("password") String password) throws Exception;
}
