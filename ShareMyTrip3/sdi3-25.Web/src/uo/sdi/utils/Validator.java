package uo.sdi.utils;

public class Validator {

	public static boolean checkNullOrEmpty(String...params) {
		for (String string : params) {
			if(string == null)
				return false;
			if(string.length() == 0)
				return false;
		}
		return true;
	}
	
	public static boolean checkNull(Object ... params){
		for (Object object : params) {
			if(object == null)
				return false;
		}
		return true;
	}
}
