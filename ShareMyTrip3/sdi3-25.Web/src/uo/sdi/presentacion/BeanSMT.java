package uo.sdi.presentacion;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import alb.util.log.Log;
import uo.sdi.business.TripService;
import uo.sdi.business.UserService;
import uo.sdi.infraestructura.Factories;
import uo.sdi.model.Trip;
import uo.sdi.model.User;
import uo.sdi.presentacion.classes.BeanFactoryImpl;
import uo.sdi.presentacion.classes.BeanLogin;
import uo.sdi.presentacion.classes.BeanPasajeros;
import uo.sdi.presentacion.classes.BeanUser;
import uo.sdi.presentacion.classes.BeanViajePromocionado;

@ManagedBean(name = "controller")
@SessionScoped
public class BeanSMT implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<Trip> viajes;
	
	private List<Trip> viajesFiltrados;

	private List<Trip> viajesUsuario;
	
	private List<Trip> viajesPromotor;
	
	private List<Trip> viajesSeleccionados;
	
	@ManagedProperty(value = "#{login}")
	private BeanLogin login;
	
	@ManagedProperty(value = "#{user}")
	private BeanUser nuevoUsuario;
	
	@ManagedProperty(value = "#{pasajeros}")
	private BeanPasajeros beanPasajeros;
	
	@ManagedProperty(value = "#{viajePromo}")
	private BeanViajePromocionado viajePromo;

	// ////////////////////
	// PARTE PUBLICA //
	// ////////////////////

	public String registroUsuario() {
		UserService service;
		try {
			service = Factories.services.getUserService();
			
			service.saveUser(nuevoUsuario);
			
			return "exito"; // Ir a login

		} catch (Exception e) {
			e.printStackTrace();
			return "error"; // Ir a la pagina de error
		}
	}

	public String listadoPublico() {
		TripService service;
		try {
			service = Factories.services.getTripService();
			viajes = service.listadoPublico();
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}

		return "exito";
	}

	// ////////////////////
	// PARTE PRIVADA //
	// ////////////////////
	
	public String iniciarSesion(){
		try {
			String verifyResult = login.verify();
			if(verifyResult.equals("fallo"))
				return null;
			else {
				return listadoUsuario();
			}
		}catch(Exception e){
			e.printStackTrace();
			return "error";
		}	
	}
	
	public String cerrarSesion(){
		FacesContext.getCurrentInstance().getExternalContext()
										 .invalidateSession();
		return "login";
			
	}

	public String listadoUsuario() {
		TripService service;
		try {
			service = Factories.services.getTripService();
			viajesUsuario = service.listadoUsuario(getUser());
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}

		return "exito";
	}

	public String listado() {
		TripService service;
		try {
			service = Factories.services.getTripService();
			viajes = service.listadoPrivado(getUser());
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
		return "exito";
	}
	
	public String apuntarse(Trip trip){
		TripService service;
		try {
			service = Factories.services.getTripService();
			service.apuntarseViaje(getUser(), trip);
			
			viajes = service.listadoPrivado(getUser());
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
		return null;
	}
	
	public String cancelarAsistencia(Trip trip){
		TripService service;
		try{
			service = Factories.services.getTripService();
			service.cancelarAsistencia(getUser(), trip);
			
			viajesUsuario = service.listadoUsuario(getUser());
			return null;
		}catch(Exception e){
			e.printStackTrace();
			return "error";
		}
	}
	
	public String registroViaje(){
		TripService service;
		try{
			service = Factories.services.getTripService();
			viajePromo.loadMap();
			service.registroViaje(getUser(), viajePromo);
			
			viajesUsuario = service.listadoUsuario(getUser());
			return "exito";
		}catch(Exception e){
			e.printStackTrace();
			return "error";
		}
	}
	
	public String listadoPromotor(){
		TripService service;
		try{
			service = Factories.services.getTripService();
			
			viajesPromotor = service.listadoPromotor(getUser());
			return "exito";
		}catch(Exception e){
			e.printStackTrace();
			return "error";
		}
	}
	
	public String cancelarViajes(ActionEvent event){
		TripService service;
		try{
			service = Factories.services.getTripService();
			
			if(viajesSeleccionados.isEmpty())
				return null;
			
			service.cancelarViaje(viajesSeleccionados);
			
			viajesPromotor = service.listadoPromotor(getUser());
			return null;
		}catch(Exception e){
			e.printStackTrace();
			return "error";
		}
	}
	
	public String editarViaje(){
		TripService service;
		try{
			service = Factories.services.getTripService();
			viajePromo.loadMap();
			service.editarViaje(viajePromo);
			
			viajesUsuario = service.listadoUsuario(getUser());
			return "exito";
		}catch(Exception e){
			e.printStackTrace();
			return "error";
		}
	}

	// Getter Setter

	public List<Trip> getViajes() {
		return viajes;
	}

	public void setViajes(List<Trip> viajes) {
		this.viajes = viajes;
	}

	private User getUser() {
		return (User) FacesContext.getCurrentInstance().getExternalContext()
				.getSessionMap().get("LOGGEDIN_USER");
	}

	public List<Trip> getViajesUsuario() {
		return viajesUsuario;
	}

	public void setViajesUsuario(List<Trip> viajesUsuario) {
		this.viajesUsuario = viajesUsuario;
	}
	
	public List<Trip> getViajesFiltrados() {
		return viajesFiltrados;
	}
	
	public List<Trip> getViajesPromotor() {
		return viajesPromotor;
	}

	public void setViajesFiltrados(List<Trip> viajesFiltrados) {
		this.viajesFiltrados = viajesFiltrados;
	}
	
	public List<Trip> getViajesSeleccionados() {
		return viajesSeleccionados;
	}
	
	public void setViajesSeleccionados(List<Trip> viajes) {
		this.viajesSeleccionados = viajes;
	}
	
	//Init / Destroy
	@PostConstruct
	public void init() {
		Log.info("BeanSMT - PostConstruct","");
		
		BeanFactory factory = new BeanFactoryImpl();
		
		nuevoUsuario = factory.getBeanUser();
		
		beanPasajeros = factory.getBeanPasajeros();
		
		login = factory.getBeanLogin();
		
		viajePromo = factory.getBeanViajePromocionado();
	}

	@PreDestroy
	public void end() {
		Log.debug("BeanSMT - PreDestroy","");
	}
}
