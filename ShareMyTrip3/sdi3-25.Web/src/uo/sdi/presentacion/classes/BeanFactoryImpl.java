package uo.sdi.presentacion.classes;

import javax.faces.context.FacesContext;

import alb.util.log.Log;
import uo.sdi.presentacion.BeanFactory;

public class BeanFactoryImpl implements BeanFactory {

	@Override
	public BeanLogin getBeanLogin() {
		Log.debug("BeanLogin - PostConstruct", "");
		BeanLogin bean = (BeanLogin) FacesContext.getCurrentInstance()
				.getExternalContext().getSessionMap().get(new String("login"));
		if (bean == null) {
			Log.debug("BeanLogin - No existia", "");
			bean = new BeanLogin();
			FacesContext.getCurrentInstance().getExternalContext()
					.getSessionMap().put("login", bean);
		}

		return bean;
	}

	@Override
	public BeanPasajeros getBeanPasajeros() {
		Log.debug("BeanPasajeros - PostConstruct", "");
		BeanPasajeros bean = (BeanPasajeros) FacesContext.getCurrentInstance()
				.getExternalContext().getSessionMap()
				.get(new String("pasajeros"));
		if (bean == null) {
			Log.debug("BeanPasajeros - No existia", "");
			bean = new BeanPasajeros();
			FacesContext.getCurrentInstance().getExternalContext()
					.getSessionMap().put("pasajeros", bean);
		}

		return bean;
	}

	@Override
	public BeanUser getBeanUser() {
		Log.debug("BeanUser - PostConstruct","");
		BeanUser user = (BeanUser) FacesContext.getCurrentInstance()
				.getExternalContext().getSessionMap().get(new String("user"));
		if (user == null) {
			Log.debug("BeanUser - User no existia","");
			user = new BeanUser();
			FacesContext.getCurrentInstance().getExternalContext()
					.getSessionMap().put("user", user);
		}
		return user;
	}

	@Override
	public BeanViajePromocionado getBeanViajePromocionado() {
		Log.debug("BeanViajePromo - PostConstruct", "");
		BeanViajePromocionado bean = (BeanViajePromocionado) FacesContext
				.getCurrentInstance().getExternalContext().getSessionMap()
				.get(new String("viajePromo"));
		if (bean == null) {
			Log.debug("BeanViajePromo - No existia", "");
			bean = new BeanViajePromocionado();
			FacesContext.getCurrentInstance().getExternalContext()
					.getSessionMap().put("viajePromo", bean);
		}

		return bean;
	}

}
