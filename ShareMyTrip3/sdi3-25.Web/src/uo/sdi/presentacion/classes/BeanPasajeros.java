package uo.sdi.presentacion.classes;

import java.io.Serializable;
import java.util.Map;

import javax.faces.bean.*;
import javax.faces.event.ActionEvent;

import uo.sdi.business.TripService;
import uo.sdi.infraestructura.Factories;
import uo.sdi.model.Trip;
import uo.sdi.model.User;

@ManagedBean(name = "pasajeros")
@SessionScoped
public class BeanPasajeros implements Serializable {

	private static final long serialVersionUID = 1L;

	private Map<String,User[]> listaPasajeros;
	private Trip trip;

	public String listadoPasajeros() {
		TripService service;

		try {
			service = Factories.services.getTripService();
			listaPasajeros = service.listadoPasajeros(trip);

			return "exito";
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
	}

	public String confirmarPasajero(User pasajero) {
		TripService service;

		try {
			service = Factories.services.getTripService();
			trip = service.confirmarPasajero(trip,pasajero);

			listaPasajeros = service.listadoPasajeros(trip);
			return null;
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
	}
	
	public String expulsar(User pasajero){
		TripService service;

		try {
			service = Factories.services.getTripService();
			trip = service.expulsar(trip,pasajero);

			listaPasajeros = service.listadoPasajeros(trip);
			return null;
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
	}

	public void setTrip(ActionEvent event, Trip trip) {
		this.trip = trip;
	}

	public void setTrip(Trip trip) {
		this.trip = trip;
	}

	public Trip getTrip() {
		return trip;
	}
	
	public Trip getTrip(ActionEvent event) {
		return trip;
	}

	public Map<String, User[]> getListaPasajeros() {
		return listaPasajeros;
	}
}
