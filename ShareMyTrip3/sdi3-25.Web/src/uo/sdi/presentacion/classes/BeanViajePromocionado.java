package uo.sdi.presentacion.classes;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import uo.sdi.model.Trip;
import uo.sdi.model.types.AddressPoint;
import uo.sdi.model.types.Waypoint;
import uo.sdi.utils.DateUtil;
import uo.sdi.utils.Validator;

@ManagedBean(name="viajePromo")
@SessionScoped
public class BeanViajePromocionado extends Trip implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private Map<String,String> departureMap;

	private Map<String,String> destinationMap;
	
	private boolean plantilla = false;
	
	public BeanViajePromocionado(){
		departureMap = new HashMap<String,String>();
		destinationMap = new HashMap<String,String>();
	}
	
	public String setTrip(Trip trip){
		try {
			setId(trip.getId());
			fillMap(departureMap, trip.getDeparture());
			fillMap(destinationMap, trip.getDestination());
			setArrivalDate(trip.getArrivalDate());
			setAvailablePax(trip.getAvailablePax());
			setClosingDate(trip.getClosingDate());
			setComments(trip.getComments());
			setDepartureDate(trip.getDepartureDate());
			setEstimatedCost(trip.getEstimatedCost());
			setMaxPax(trip.getMaxPax());
			
			return "exito";
		} catch(Exception e){
			return "error";
		}
	}
	
	public Map<String, String> getDepartureMap() {
		return departureMap;
	}

	public Map<String, String> getDestinationMap() {
		return destinationMap;
	}

	private void fillMap(Map<String,String> map, AddressPoint ap){
		String direccion = ap.getAddress();
		String ciudad = ap.getCity();
		String provincia = ap.getState();
		String pais = ap.getCountry();
		String cp = ap.getZipCode();
		
		map.put("direccion",direccion);
		map.put("ciudad",ciudad);
		map.put("provincia",provincia);
		map.put("pais",pais);
		map.put("cp",cp);
		
		Waypoint wp = ap.getWaypoint();
		
		if(wp != null){
			String lat = wp.getLat().toString();
			String lon = wp.getLon().toString();
			map.put("lat",lat);
			map.put("lon",lon);
		}
	}
	
	public void clear(){
		setArrivalDate(null);
		setAvailablePax(0);
		setClosingDate(null);
		setComments(null);
		setDepartureDate(null);
		setEstimatedCost(0.0);
		setMaxPax(0);
		departureMap.clear();
		destinationMap.clear();
	}

	public boolean isPlantilla() {
		return plantilla;
	}

	public void setPlantilla(boolean plantilla) {
		this.plantilla = plantilla;
	}
	
	public void loadPlantilla(){
		if(plantilla){
			plantilla();
		} else {
			clear();
		}
			
	}
	
	private void plantilla(){
		AddressPoint salida = new AddressPoint("direccion salida", "ciudad salida",
				"provincia salida", "pais salida", "33400", new Waypoint(-33.00, 20.0));
		AddressPoint llegada = new AddressPoint("direccion llegada", "ciudad llegada",
				"provincia llegada", "pais llegada", "27481", new Waypoint(-20.00, 20.0));
		
		fillMap(departureMap, salida);
		fillMap(destinationMap, llegada);
		
		Date arrival = DateUtil.dateFromNowOnTo(4); 
		Date closing = DateUtil.dateFromNowOnTo(2); 
		Date departure = DateUtil.dateFromNowOnTo(3); 
		
		setArrivalDate(arrival);
		setAvailablePax(5);
		setClosingDate(closing);
		setComments("buen viaje");
		setDepartureDate(departure);
		setEstimatedCost(55.0);
		setMaxPax(5);
	}
	
	private AddressPoint getAddressPoint(Map<String,String> map){
		String direccion = map.get("direccion");
		String ciudad = map.get("ciudad");
		String provincia = map.get("provincia");
		String pais = map.get("pais");
		String cp = map.get("cp");
		String lat = map.get("lat");
		String lon = map.get("lon");
		
		Waypoint waypoint = null;
		if(Validator.checkNullOrEmpty(lat,lon)){
			waypoint = new Waypoint(Double.valueOf(lat), Double.valueOf(lon));
		}
		
		return new AddressPoint(direccion, ciudad, provincia, pais, cp, waypoint);
	}
	
	public void loadMap(){
		setDeparture(getAddressPoint(departureMap));
		setDestination(getAddressPoint(destinationMap));
	}
}
