package uo.sdi.presentacion.classes;

import java.awt.event.ActionEvent;
import java.io.Serializable;
import java.util.ResourceBundle;

import javax.faces.bean.*;
import javax.faces.context.FacesContext;

import uo.sdi.model.User;
import uo.sdi.model.types.UserStatus;


@ManagedBean(name="user")
@SessionScoped
public class BeanUser extends User implements Serializable {

	private static final long serialVersionUID = 55556L;

	public BeanUser() {
		iniciaUser(null);
	}

	public void setAlumno(User user) {
		setId(user.getId());
		setLogin(user.getLogin());
		setName(user.getName());
		setSurname(user.getSurname());
		setEmail(user.getEmail());
		setPassword(user.getPassword());
		setStatus(user.getStatus());
	}

	public void iniciaUser(ActionEvent event) {
		FacesContext facesContext = FacesContext.getCurrentInstance();
		ResourceBundle bundle = 
				facesContext.getApplication().
				getResourceBundle(facesContext, "msgs");		
		setId(null);
		setLogin(bundle.getString("valorDefectoLogin"));
		setName(bundle.getString("valorDefectoNombre"));
		setSurname(bundle.getString("valorDefectoApellidos"));
		setEmail(bundle.getString("valorDefectoCorreo"));
		setPassword("");
		setStatus(UserStatus.ACTIVE);
	}
}
