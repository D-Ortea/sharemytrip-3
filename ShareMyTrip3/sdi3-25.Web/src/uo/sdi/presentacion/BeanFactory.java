package uo.sdi.presentacion;

import uo.sdi.presentacion.classes.BeanLogin;
import uo.sdi.presentacion.classes.BeanPasajeros;
import uo.sdi.presentacion.classes.BeanUser;
import uo.sdi.presentacion.classes.BeanViajePromocionado;

public interface BeanFactory {

	BeanLogin getBeanLogin();
	BeanPasajeros getBeanPasajeros();
	BeanUser getBeanUser();
	BeanViajePromocionado getBeanViajePromocionado();
}
