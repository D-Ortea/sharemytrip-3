package uo.sdi.presentacion.validators;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

import uo.sdi.infraestructura.Factories;

@FacesValidator("emailValidator")
public class EmailValidator implements Validator {

	@Override
	public void validate(FacesContext context, UIComponent component,
			Object value) throws ValidatorException {
		try {

			String login = (String) value;
			
			if(!Factories.services.getUserService().isLoginAvailable(login))
				throw new RuntimeException();
			
		} catch (Exception e) {
			FacesMessage msg = new FacesMessage("E-mail validation failed.",
					"Invalid E-mail format.");
			msg.setSeverity(FacesMessage.SEVERITY_ERROR);
			throw new ValidatorException(msg);
		}
	}
}
