package uo.sdi.client.menu;

import uo.sdi.client.util.BaseMenu;

public class LoginMenu extends BaseMenu {

    public LoginMenu() {
	menuOptions = new Object[][] {
	    { "Share My Trip REST Client", null },
	    
	    { "Iniciar Sesión",          MainMenu.class}
	};
    }

    public static void main(String[] args) {
    	new LoginMenu().execute();
    }

}