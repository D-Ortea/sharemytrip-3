package uo.sdi.client.menu;

import alb.util.console.Console;
import uo.sdi.client.action.ConfirmarPasajeros;
import uo.sdi.client.model.User;
import uo.sdi.client.rest.RestClient;
import uo.sdi.client.util.BaseMenu;

public class MainMenu extends BaseMenu {

	public MainMenu() {
		menuOptions = new Object[][] { 
			{ "Panel de usuario" , null },

			{ "Confirmar Pasajeros",       ConfirmarPasajeros.class}
		};
	}
	
	@Override
	public void execute() {
		//Pedir datos
		String username = Console.readString("Usuario");
		String password = Console.readString("Contraseña");
		
		RestClient.createRestService(username,password);
		
		try {
			User user = RestClient.getService().login(username, password);
			if(user == null)
				throw new RuntimeException();
			RestClient.setValidatedUser(user);
		}catch(Exception e){
			Console.println("Login fallido");
			return;
		}
		
		//Dejarlo para llamar al menu
		super.execute();
	}
}
