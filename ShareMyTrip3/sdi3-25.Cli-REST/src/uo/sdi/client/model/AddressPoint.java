package uo.sdi.client.model;

public class AddressPoint{
	
	private String address;
	private String city;
	private String state;
	private String country;
	private String zipCode;
	
	private Waypoint waypoint;
	
	public AddressPoint() { }
	
	public AddressPoint(String address, String city, String state, 
			String country, String zipCode, Waypoint waypoint) 
	{	
		super();
		this.address = address;
		this.city = city;
		this.state = state;
		this.country = country;
		this.zipCode = zipCode;
		this.waypoint = waypoint;
	}

	public String getAddress() {
		return address;
	}

	public String getCity() {
		return city;
	}

	public String getState() {
		return state;
	}

	public String getCountry() {
		return country;
	}

	public String getZipCode() {
		return zipCode;
	}

	public Waypoint getWaypoint() {
		return waypoint;
	}
	
	@Override
	public String toString() {
		String result =  country + " - " + state + "\n" +
			   address + " (" + city + ")";
		if(waypoint != null)
			result += "\n" + "["+ waypoint.toString()+ "]";
		return result;
	}
}
