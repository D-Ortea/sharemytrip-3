package uo.sdi.client.action;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.core.Response;

import alb.util.console.Console;
import uo.sdi.client.model.Trip;
import uo.sdi.client.model.User;
import uo.sdi.client.rest.RestClient;
import uo.sdi.client.util.Action;
import uo.sdi.client.util.Printer;

public class ConfirmarPasajeros implements Action {

	@Override
	public void execute() throws Exception {

		// mostrar viajes
		List<Trip> trips = RestClient.getService().getPromotedTrips(
				RestClient.getValidatedUser().getId());
		
		List<Trip> available = new ArrayList<Trip>();
		
		for (Trip trip : trips) {
			if(trip.getAvailablePax() != 0)
				available.add(trip);
		}

		Console.println("Aqui se muestran sus viajes como promotor con plazas libres");

		Printer.printTrips(available);

		Long tripId = Console.readLong("Introduzca el id del viaje");
		if (tripId == null) {
			Console.print("Introduzca un id de viaje correcto");
			return;
		}

		if (!checkTrip(tripId, available)) {
			Console.printf("El id [%d] no encuentra en la lista de viajes",
					tripId);
			return;
		}

		// mostrar usuarios admitidos
		List<User> users = RestClient.getService().getPasajerosSinConfirmar(
				tripId);

		if (users.isEmpty()) {
			Console.printf("No hay usuarios apuntados en el viaje [%d]", tripId);
			return;
		}

		if (checkAvailablePax(tripId, trips)) {
			Console.printf("El viaje [%d] no tiene plazas disponibles", tripId);
		}

		Console.println("Ahora se muestra los usuarios sin confirmar");

		Printer.printUsers(users);

		Long userId = Console
				.readLong("Introduzca el id del usuario a confirmar");
		if (userId == null) {
			Console.print("Introduzca un id de viaje correcto");
			return;
		}

		if (!checkUser(userId, users)) {
			Console.printf("El id [%d] no encuentra en la lista de usuarios",
					userId);
			return;
		}

		// Confirmar
		Response r = RestClient.getService().confirmarPasajero(tripId, userId);
		if (Response.Status.fromStatusCode(r.getStatus()).equals(
				Response.Status.OK))
			Console.printf("Usuario [%d] confirmado para viaje [%d]", userId,
					tripId);
		else
			Console.printf("Ha ocurrido un error procesando "
					+ "la confirmación del usuario [%d] para el viaje [%d]",
					userId, tripId);
		r.close();
	}

	private boolean checkAvailablePax(Long tripId, List<Trip> trips) {
		// Comprobar si esta en la lista
		for (Trip trip : trips) {
			if (trip.equals(tripId))
				return (trip.getAvailablePax() != 0);
		}

		return false;
	}

	private boolean checkUser(Long userId, List<User> users) {
		// Comprobar si esta en la lista
		for (User user : users) {
			if (user.getId().equals(userId))
				return true;
		}

		return false;
	}

	private boolean checkTrip(Long tripId, List<Trip> trips) {
		// Comprobar si esta en la lista
		for (Trip trip : trips) {
			if (trip.getId().equals(tripId))
				return true;
		}

		return false;
	}
}
