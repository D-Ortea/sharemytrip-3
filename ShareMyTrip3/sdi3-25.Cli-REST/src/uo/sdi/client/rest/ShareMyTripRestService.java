package uo.sdi.client.rest;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import uo.sdi.client.model.Trip;
import uo.sdi.client.model.User;

@Path("/rest")
public interface ShareMyTripRestService {

	@GET
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Path("/promotor/{id}")
	List<Trip> getPromotedTrips(@PathParam("id") Long id);

	@GET
	@Path("/pasajeros/{id}")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	List<User> getPasajerosSinConfirmar(@PathParam("id") Long id);

	@POST
	@Path("/promotor/confirmar")
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	Response confirmarPasajero(@QueryParam("trip") Long tripId,
			@QueryParam("user") Long userId);
	
	@GET
	@Path("/login")
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	User login(@QueryParam("username") String username,
			@QueryParam("password") String password) throws Exception;
}
