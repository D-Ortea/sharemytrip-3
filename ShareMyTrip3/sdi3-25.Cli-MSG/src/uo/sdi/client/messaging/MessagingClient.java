package uo.sdi.client.messaging;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;

import uo.sdi.client.util.Jndi;

public class MessagingClient {

	private static final String JMS_CONNECTION_FACTORY = "jms/RemoteConnectionFactory";
	private static final String SMT_QUEUE = "jms/queue/smt-inputChannel";
	//private static final String SMT_QUEUE = "jms/queue/NotaneitorQueue";
	private static final String SMT_TOPIC = "jms/topic/smt-subscription";
	
	private static ConnectionFactory factory;

	private static Destination queue;
	private static Destination topic;

	private final static String USERNAME = "sdi";
	private final static String PASSWORD = "password";

	private MessagingClient() {
	}

	public static void createMessagingClient() {
		factory = (ConnectionFactory) Jndi.find(JMS_CONNECTION_FACTORY);
		queue = (Destination) Jndi.find(SMT_QUEUE);
		topic = (Destination) Jndi.find(SMT_TOPIC);
	}

	public static Connection getConnection() throws JMSException {
		return factory.createConnection(USERNAME, PASSWORD);
	}

	public static Destination getQueue() {
		return queue;
	}
	
	public static Destination getTopic(){
		return topic;
	}
}
