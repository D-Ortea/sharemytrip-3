package uo.sdi.client.messaging;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.jms.JMSException;
import javax.jms.MapMessage;

public class MessageLogger {

	public final static String LOG_PATH = "./records/";
	public final static String FILE_HEADER_NAME = "trip#";
	
	public MessageLogger(){
		
	}
	
	public static List<String> getStoredMessages(Long tripId) throws IOException{
		String filename = FILE_HEADER_NAME + tripId;
		
		List<String> messages = new ArrayList<String>();
		
		File file = new File(LOG_PATH+filename);
		
		if(!file.exists())
			return messages;
		
		BufferedReader reader = new BufferedReader(new FileReader(file));
		
		while(reader.ready()){
			messages.add(reader.readLine());
		}
		
		reader.close();
		
		return messages;
	}
	
	public static void storeMessage(MapMessage mmsg) throws JMSException, IOException{
		String message = String.format("[%s] %s: %s", new Date(mmsg.getJMSTimestamp()),
				mmsg.getString("username"),mmsg.getString("content"));
		
		String filename = FILE_HEADER_NAME + mmsg.getLong("tripId");
		
		File file = new File(LOG_PATH+filename);
		
		BufferedWriter output;
		if(file.exists())
			output = new BufferedWriter(new FileWriter(file,true));
		else
			output = new BufferedWriter(new FileWriter(file));
		
		output.write(message);
		output.newLine();
		
		output.close();
	}
}
