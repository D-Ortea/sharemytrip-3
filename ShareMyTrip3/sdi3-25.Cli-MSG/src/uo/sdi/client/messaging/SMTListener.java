package uo.sdi.client.messaging;

import java.io.IOException;

import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.Message;
import javax.jms.MessageListener;

import alb.util.console.Console;

public class SMTListener implements MessageListener {

	@Override
	public void onMessage(Message msg) {
		try {
			processMessage(msg);
		} catch (JMSException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void processMessage(Message msg) throws JMSException, IOException{
		if (!(msg instanceof MapMessage)) {
			Console.println("Message not of expected type");
			return;
		}
		
		MessageLogger.storeMessage((MapMessage) msg);
	}

}
