package uo.sdi.client.messaging;

import javax.jms.Connection;
import javax.jms.JMSException;
import javax.jms.MessageConsumer;
import javax.jms.Session;

public class SMTMessageReciever {

	private Connection con;
	private Session session;
	private MessageConsumer consumer;

	public SMTMessageReciever() {
	}

	public void start() {
		try {
			con = MessagingClient.getConnection();
			session = con.createSession(false, Session.AUTO_ACKNOWLEDGE);
			consumer = session.createConsumer(MessagingClient.getTopic());

			consumer.setMessageListener(new SMTListener());
			
			con.start();
		} catch (JMSException e) {
			throw new RuntimeException(e);
		}
	}

	public void close() {
		try {
			if (consumer != null)
				consumer.close();

			if (session != null)
				session.close();

			if (con != null)
				con.close();
		} catch (JMSException e) {
			
		}
	}
}
