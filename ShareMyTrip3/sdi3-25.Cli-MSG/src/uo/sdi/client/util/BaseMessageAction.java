package uo.sdi.client.util;

import javax.jms.Connection;
import javax.jms.JMSException;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;
import javax.jms.Session;

import alb.util.console.Console;
import uo.sdi.client.messaging.MessagingClient;

public abstract class BaseMessageAction implements Action{
	
	protected Connection con;
	protected Session session;
	protected MessageProducer producer;
	protected MessageConsumer consumer;
	
	private void initialize() throws JMSException {
		con = MessagingClient.getConnection();
	}
	
	@Override
	public void execute() throws Exception {
		
		try{
			initialize();
		
			doExecute();
		
			close();
		}catch(JMSException e){
			Console.printf("Error de JMS [%s]",e.toString());
		}
	}

	private void close() throws JMSException {
		if(producer != null)
			producer.close();
		
		if(consumer != null)
			consumer.close();
		
		if(session != null)
			session.close();
		
		if(con != null)
			con.close();
	}
	
	public abstract void doExecute() throws JMSException;
}
