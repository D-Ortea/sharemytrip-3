package uo.sdi.client.util;

import java.util.List;

import alb.util.console.Console;
import de.vandermeer.asciitable.v2.V2_AsciiTable;
import de.vandermeer.asciitable.v2.render.V2_AsciiTableRenderer;
import de.vandermeer.asciitable.v2.render.WidthAbsoluteEven;
import de.vandermeer.asciitable.v2.themes.V2_E_TableThemes;
import uo.sdi.client.model.Trip;
import uo.sdi.client.model.User;

public class Printer {

	public static void printTrips(List<Trip> trips) {
		V2_AsciiTable at = new V2_AsciiTable();

		at.addStrongRule();
		at.addRow("Id", "Salida", "Destino", "Fecha Salida", "Fecha Llegada",
				"Plazas Disponibles", "Comentario");
		at.addStrongRule();

		for (Trip trip : trips) {
			listTrip(trip, at);
		}
		printTable(at);
	}
	
	public static void printUsers(List<User> users) {
		V2_AsciiTable at = new V2_AsciiTable();

		at.addStrongRule();
		at.addRow("Id", "Login", "Nombre", "Apellidos", "Email");
		at.addStrongRule();
		
		for (User user : users) {
			listUser(user, at);
		}
		at.addStrongRule();
		printTable(at);
	}

	private static void listTrip(Trip trip, V2_AsciiTable at) {
		at.addRow(trip.getId(), trip.getDeparture().toString(), trip
				.getDestination().toString(), trip.getDepartureDate(), trip
				.getArrivalDate(), trip.getAvailablePax(), trip.getComments());
		at.addRule();
	}
	
	private static void listUser(User user, V2_AsciiTable at){
		at.addRow(user.getId(),user.getLogin(), user.getName(),
				user.getSurname(),user.getEmail());
	}

	private static void printTable(V2_AsciiTable table) {
		V2_AsciiTableRenderer rend = new V2_AsciiTableRenderer();
		rend.setTheme(V2_E_TableThemes.UTF_LIGHT.get());
		rend.setWidth(new WidthAbsoluteEven(200));
		System.out.println(rend.render(table));
	}

	public static void printLog(List<String> messages, Long tripId) {
		Console.printf("Mensajes del viaje #%d\n",tripId);
		Console.println("-------------------------------------------------------");
		for (String msg : messages) {
			Console.println(msg);
		}
		Console.println("-------------------------------------------------------");
		
		
	}
}
