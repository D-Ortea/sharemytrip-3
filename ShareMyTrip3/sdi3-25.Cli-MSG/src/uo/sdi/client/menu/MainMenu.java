package uo.sdi.client.menu;

import alb.util.console.Console;
import uo.sdi.client.action.EscribirMensajeAction;
import uo.sdi.client.action.LeerMensajesAction;
import uo.sdi.client.messaging.MessagingClient;
import uo.sdi.client.messaging.SMTMessageReciever;
import uo.sdi.client.model.User;
import uo.sdi.client.rest.RestClient;
import uo.sdi.client.util.BaseMenu;

public class MainMenu extends BaseMenu {
	
	public MainMenu() {
	menuOptions = new Object[][] {
		{ "Share My Trip MSG Client", null },
		
		{ "Escribir Mensajes",        EscribirMensajeAction.class },
		{ "Leer Mensajes",            LeerMensajesAction.class    },
	};
	}
	
	@Override
	public void execute() {
		String username = Console.readString("Usuario");
		String password = Console.readString("Contraseña");
		
		RestClient.createRestService(username,password);
		
		try {
			User user = RestClient.getService().login(username, password);
			if(user == null)
				throw new RuntimeException();
			RestClient.setValidatedUser(user);
		}catch(Exception e){
			Console.println("Login fallido");
			return;
		}
		
		MessagingClient.createMessagingClient();
		
		SMTMessageReciever reciever = new SMTMessageReciever();
		reciever.start();
		
		// No quitar, esto llama al menu
		super.execute();
		
		reciever.close();
	}
}
