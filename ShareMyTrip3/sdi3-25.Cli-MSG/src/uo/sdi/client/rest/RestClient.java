package uo.sdi.client.rest;

import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;

import uo.sdi.client.model.User;

/**
 * Rest Client Singleton
 */
public class RestClient {
	private static ShareMyTripMessagingRestService service = null;

	private static User validatedUser = null;

	private RestClient() {
	}

	public static void createRestService(String username, String password){
		service = new ResteasyClientBuilder().build()
				.register(new Authenticator(username, password))
				.target("http://localhost:8280/sdi3-25.Web/")
				.proxy(ShareMyTripMessagingRestService.class);
	}

	public static ShareMyTripMessagingRestService getService() {
		return service;
	}

	public static User getValidatedUser() {
		return validatedUser;
	}
	
	public static void setValidatedUser(User user){
		validatedUser = user;
	}
}
