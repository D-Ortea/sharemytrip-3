package uo.sdi.client.action;

import java.util.List;

import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.Message;
import javax.jms.Session;

import alb.util.console.Console;
import uo.sdi.client.messaging.MessagingClient;
import uo.sdi.client.model.Trip;
import uo.sdi.client.model.User;
import uo.sdi.client.rest.RestClient;
import uo.sdi.client.util.BaseMessageAction;
import uo.sdi.client.util.Printer;

public class EscribirMensajeAction extends BaseMessageAction {

	@Override
	public void doExecute() throws JMSException {

		// mostrar viajes y pedir
		Console.println("Aqui se muestran los viajes en los que participas o has participado");

		List<Trip> trips = RestClient.getService().participacionActiva(
				RestClient.getValidatedUser().getId());

		Printer.printTrips(trips);

		Long tripId = Console.readLong("Introduzca el id del viaje");
		if (tripId == null) {
			Console.print("Introduzca un id de viaje correcto");
			return;
		}

		if (!checkTrip(tripId, trips)) {
			Console.printf("El id [%d] no encuentra en la lista de viajes",
					tripId);
			return;
		}

		// Pedir mensaje

		String messageContent = null;
		while (messageContent == null || messageContent.isEmpty()) {
			messageContent = Console.readString("Introduce el mensaje");
		}

		// Crear y Mandar mensaje

		session = con.createSession(false, Session.AUTO_ACKNOWLEDGE);
		producer = session.createProducer(MessagingClient.getQueue());
		con.start();

		Message msg = generateMessage(messageContent, tripId,
				RestClient.getValidatedUser());

		producer.send(msg);
		
		Console.println("Mensaje Enviado");
	}

	private Message generateMessage(String content, Long tripId, User user)
			throws JMSException 
	{
		MapMessage msg = session.createMapMessage();

		msg.setLong("tripId", tripId);
		msg.setString("content", content);
		msg.setLong("userId", user.getId());
		msg.setString("username", user.getLogin());

		return msg;
	}

	private boolean checkTrip(Long tripId, List<Trip> trips) {
		// Comprobar si esta en la lista
		for (Trip trip : trips) {
			if (trip.getId().equals(tripId))
				return true;
		}

		return false;
	}
}
