package uo.sdi.client.action;

import java.io.IOException;
import java.util.List;

import alb.util.console.Console;
import uo.sdi.client.messaging.MessageLogger;
import uo.sdi.client.model.Trip;
import uo.sdi.client.rest.RestClient;
import uo.sdi.client.util.BaseMessageAction;
import uo.sdi.client.util.Printer;

public class LeerMensajesAction extends BaseMessageAction {

	@Override
	public void doExecute() {
		// mostrar viajes y pedir
		Console.println("Aqui se muestran los viajes en los que participas o has participado");

		List<Trip> trips = RestClient.getService().participacionActiva(
				RestClient.getValidatedUser().getId());

		Printer.printTrips(trips);

		Long tripId = Console.readLong("Introduzca el id del viaje");
		if (tripId == null) {
			Console.print("Introduzca un id de viaje correcto");
			return;
		}

		if (!checkTrip(tripId, trips)) {
			Console.printf("El id [%d] no encuentra en la lista de viajes",
					tripId);
			return;
		}
		
		//Leer mensajes
		List<String> messages;
		try {
			messages = MessageLogger.getStoredMessages(tripId);
			if(messages.isEmpty()) {
				Console.printf("El viaje [%d] no tiene comentarios todavía",tripId);
				return;
			}
			Printer.printLog(messages, tripId);
		} catch (IOException e) {
			Console.println("Error procesando mensajes");
		}
	}
	
	private boolean checkTrip(Long tripId, List<Trip> trips) {
		// Comprobar si esta en la lista
		for (Trip trip : trips) {
			if (trip.getId().equals(tripId))
				return true;
		}

		return false;
	}

}
