package uo.sdi.client.model;

public class Waypoint{
	
	private Double lat;
	private Double lon;
	
	public Waypoint() { }
	
	public Waypoint(Double lat, Double lon) {
		super();
		this.lat = lat;
		this.lon = lon;
	}

	public Double getLat() {
		return lat;
	}

	public Double getLon() {
		return lon;
	}
	
	@Override
	public String toString() {
		return lat + ", " + lon;
	}
}
