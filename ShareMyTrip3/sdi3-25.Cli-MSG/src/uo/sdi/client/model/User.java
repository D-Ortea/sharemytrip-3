package uo.sdi.client.model;

public class User {

	private Long id;

	private String login;
	private String name;
	private String surname;
	private String email;
	
	public User() {}

	public User(Long id, String login, String name, String surname, String email)
	{
		super();
		this.id = id;
		this.login = login;
		this.name = name;
		this.surname = surname;
		this.email = email;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
}
