package uo.sdi.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "TRATINGS")
public class Rating implements Serializable{

	private static final long serialVersionUID = 6556735600920411359L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private String comment;
	private Integer value = 0;
	
	@Transient
	private String[] dataSoap;

	/* Cross Reference */
	@ManyToOne
	private Seat from;
	@ManyToOne
	private Seat about;
	
	public Rating(){ }
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public Integer getValue() {
		return value;
	}

	public void setValue(Integer value) {
		this.value = value;
	}

	public Seat getFrom() {
		return from;
	}

	public Seat getAbout() {
		return about;
	}

	public String[] getDataSoap() {
		return dataSoap;
	}

	public void setDataSoap(String[] dataSoap) {
		this.dataSoap = dataSoap;
	}
}
