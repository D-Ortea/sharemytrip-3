package uo.sdi.model;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import uo.sdi.model.types.AddressPoint;
import uo.sdi.model.types.TripStatus;
import uo.sdi.model.types.TripType;
import uo.sdi.utils.DateUtil;

@Entity
@Table(name = "TTRIPS")
@XmlRootElement(name = "viaje")
public class Trip implements Serializable {

	private static final long serialVersionUID = -6415989582658531522L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Embedded
	@AttributeOverrides({
		@AttributeOverride(column = @Column(name = "DEPARTURE_ADDRESS"), name = "address"),
		@AttributeOverride(column = @Column(name = "DEPARTURE_CITY"), name = "city"),
		@AttributeOverride(column = @Column(name = "DEPARTURE_STATE"), name = "state"),
		@AttributeOverride(column = @Column(name = "DEPARTURE_COUNTRY"), name = "country"),
		@AttributeOverride(column = @Column(name = "DEPARTURE_ZIPCODE"), name = "zipCode"),
		@AttributeOverride(column = @Column(name = "DEPARTURE_WPT_LAT"), name = "waypoint.lat"),
		@AttributeOverride(column = @Column(name = "DEPARTURE_WPT_LON"), name = "waypoint.lon") })
	private AddressPoint departure;

	@Embedded
	@AttributeOverrides({
		@AttributeOverride(column = @Column(name = "DESTINATION_ADDRESS"), name = "address"),
		@AttributeOverride(column = @Column(name = "DESTINATION_CITY"), name = "city"),
		@AttributeOverride(column = @Column(name = "DESTINATION_STATE"), name = "state"),
		@AttributeOverride(column = @Column(name = "DESTINATION_COUNTRY"), name = "country"),
		@AttributeOverride(column = @Column(name = "DESTINATION_ZIPCODE"), name = "zipCode"),
		@AttributeOverride(column = @Column(name = "DESTINATION_WPT_LAT"), name = "waypoint.lat"),
		@AttributeOverride(column = @Column(name = "DESTINATION_WPT_LON"), name = "waypoint.lon") })
	private AddressPoint destination;

	@Temporal(TemporalType.TIMESTAMP)
	private Date arrivalDate;

	@Temporal(TemporalType.TIMESTAMP)
	private Date departureDate;

	@Temporal(TemporalType.TIMESTAMP)
	private Date closingDate;

	private Integer availablePax = 0;
	private Integer maxPax = 0;
	private Double estimatedCost = 0.0;
	private String comments;

	@Enumerated(EnumType.ORDINAL)
	private TripStatus status;

	/* Cross Refrence */
	@ManyToOne
	@JoinColumn(name = "PROMOTER_ID")
	private User promoter;

	@OneToMany(mappedBy = "trip")
	private Set<Seat> seats = new HashSet<Seat>();

	@OneToMany(mappedBy = "trip")
	private Set<Application> application = new HashSet<Application>();
	
	@Transient
	private TripType type;
	
	public void setType(TripType type){
		this.type = type;
	}
	
	@XmlTransient
	public TripType getType(){
		return this.type;
	}
	
	@XmlTransient
	public Set<Seat> getSeats() {
		return new HashSet<Seat>(seats);
	}

	protected Set<Seat> _getSeats() {
		return seats;
	}

	@XmlTransient
	public Set<Application> getApplication() {
		return new HashSet<Application>(application);
	}

	protected Set<Application> _getApplication() {
		return application;
	}

	/* Constructors */
	public Trip() {
	}

	public Trip(AddressPoint departure, AddressPoint destination,
			Date arrivalDate, Date departureDate, Date closingDate,
			Integer availablePax, Integer maxPax, Double estimatedCost,
			String comments, TripStatus status, User promoter)
	{
		super();
		this.departure = departure;
		this.destination = destination;
		this.arrivalDate = arrivalDate;
		this.departureDate = departureDate;
		this.closingDate = closingDate;
		this.availablePax = availablePax;
		this.maxPax = maxPax;
		this.estimatedCost = estimatedCost;
		this.comments = comments;
		this.status = status;
		this.promoter = promoter;
	}

	/* Model Data */
	@XmlElement
	public AddressPoint getDeparture() {
		return departure;
	}

	public void setDeparture(AddressPoint departure) {
		this.departure = departure;
	}

	@XmlTransient
	public TripStatus getStatus() {
		return status;
	}

	public void setStatus(TripStatus status) {
		this.status = status;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@XmlElement
	public AddressPoint getDestination() {
		return destination;
	}

	public void setDestination(AddressPoint destination) {
		this.destination = destination;
	}

	public Date getArrivalDate() {
		return arrivalDate;
	}

	public void setArrivalDate(Date arrivalDate) {
		this.arrivalDate = arrivalDate;
	}

	public Date getDepartureDate() {
		return departureDate;
	}

	public void setDepartureDate(Date departureDate) {
		this.departureDate = departureDate;
	}

	public Date getClosingDate() {
		return closingDate;
	}

	public void setClosingDate(Date closingDate) {
		this.closingDate = closingDate;
	}

	public Integer getAvailablePax() {
		return availablePax;
	}

	public void setAvailablePax(Integer availablePax) {
		this.availablePax = availablePax;
	}

	public Integer getMaxPax() {
		return maxPax;
	}

	public void setMaxPax(Integer maxPax) {
		this.maxPax = maxPax;
	}

	public Double getEstimatedCost() {
		return estimatedCost;
	}

	public void setEstimatedCost(Double estimatedCost) {
		this.estimatedCost = estimatedCost;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	@XmlTransient
	public User getPromoter() {
		return promoter;
	}

	public void setPromoter(User promoter) {
		this.promoter = promoter;
	}

	@XmlTransient
	public boolean isNotOver(){
		if(DateUtil.isBefore(closingDate,DateUtil.today()))
			return false;
		if(!status.equals(TripStatus.OPEN))
			return false;
		return true;
	}

	public boolean disponible() {
		if(DateUtil.isBefore(closingDate,DateUtil.today()))
			return false;
		if(!status.equals(TripStatus.OPEN))
			return false;
		if(availablePax == 0)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Trip [id=" + id + ", departure=" + departure + ", destination="
				+ destination + ", arrivalDate=" + arrivalDate
				+ ", departureDate=" + departureDate + ", closingDate="
				+ closingDate + ", availablePax=" + availablePax + ", maxPax="
				+ maxPax + ", estimatedCost=" + estimatedCost + ", comments="
				+ comments + ", status=" + status + ", promoterId=" + promoter
				+ "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((arrivalDate == null) ? 0 : arrivalDate.hashCode());
		result = prime * result
				+ ((availablePax == null) ? 0 : availablePax.hashCode());
		result = prime * result
				+ ((closingDate == null) ? 0 : closingDate.hashCode());
		result = prime * result
				+ ((comments == null) ? 0 : comments.hashCode());
		result = prime * result
				+ ((departure == null) ? 0 : departure.hashCode());
		result = prime * result
				+ ((departureDate == null) ? 0 : departureDate.hashCode());
		result = prime * result
				+ ((destination == null) ? 0 : destination.hashCode());
		result = prime * result
				+ ((estimatedCost == null) ? 0 : estimatedCost.hashCode());
		result = prime * result + ((maxPax == null) ? 0 : maxPax.hashCode());
		result = prime * result
				+ ((promoter == null) ? 0 : promoter.hashCode());
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Trip other = (Trip) obj;
		if (arrivalDate == null) {
			if (other.arrivalDate != null)
				return false;
		} else if (!arrivalDate.equals(other.arrivalDate))
			return false;
		if (availablePax == null) {
			if (other.availablePax != null)
				return false;
		} else if (!availablePax.equals(other.availablePax))
			return false;
		if (closingDate == null) {
			if (other.closingDate != null)
				return false;
		} else if (!closingDate.equals(other.closingDate))
			return false;
		if (comments == null) {
			if (other.comments != null)
				return false;
		} else if (!comments.equals(other.comments))
			return false;
		if (departure == null) {
			if (other.departure != null)
				return false;
		} else if (!departure.equals(other.departure))
			return false;
		if (departureDate == null) {
			if (other.departureDate != null)
				return false;
		} else if (!departureDate.equals(other.departureDate))
			return false;
		if (destination == null) {
			if (other.destination != null)
				return false;
		} else if (!destination.equals(other.destination))
			return false;
		if (estimatedCost == null) {
			if (other.estimatedCost != null)
				return false;
		} else if (!estimatedCost.equals(other.estimatedCost))
			return false;
		if (maxPax == null) {
			if (other.maxPax != null)
				return false;
		} else if (!maxPax.equals(other.maxPax))
			return false;
		if (promoter == null) {
			if (other.promoter != null)
				return false;
		} else if (!promoter.equals(other.promoter))
			return false;
		if (status != other.status)
			return false;
		return true;

	}
}