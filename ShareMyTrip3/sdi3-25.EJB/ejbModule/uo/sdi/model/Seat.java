package uo.sdi.model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import uo.sdi.model.types.SeatStatus;

@Entity
@IdClass(SeatKey.class)
@Table(name = "TSEATS")
public class Seat implements Serializable{

	private static final long serialVersionUID = 920533134957688219L;
	
	@Id
	@ManyToOne
	@JoinColumn(name = "USER_ID")
	private User user;
	@Id
	@ManyToOne
	@JoinColumn(name = "TRIP_ID")
	private Trip trip;

	private String comment;

	@Enumerated(EnumType.ORDINAL)
	private SeatStatus status;
	
	/* Cross Reference */
	@OneToMany(mappedBy="about")
	private Set<Rating> aboutRatings;
	
	@OneToMany(mappedBy="from")
	private Set<Rating> fromRatings;
	
	public Set<Rating> getAboutRatings() {
		return new HashSet<Rating>(aboutRatings);
	}

	public Set<Rating> getFromRatings() {
		return new HashSet<Rating>(fromRatings);
	}

	public Seat(){}
	
	public Seat(User user, Trip trip) {
		this.trip = trip;
		this.user = user;
		trip.setAvailablePax(trip.getAvailablePax() - 1);
		this.comment ="";
		this.status = SeatStatus.ACCEPTED;
	}

	public Long[] makeKey() {
		return new Long[] { user.getId(), trip.getId() };
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Trip getTrip() {
		return trip;
	}

	public void setTrip(Trip trip) {
		this.trip = trip;
	}
	
	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public SeatStatus getStatus() {
		return status;
	}

	public void setStatus(SeatStatus status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "Seat [user=" + user + ", trip=" + trip + ", comment=" + comment
				+ ", status=" + status + "]";
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((trip == null) ? 0 : trip.hashCode());
		result = prime * result + ((user == null) ? 0 : user.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Seat other = (Seat) obj;
		if (trip == null) {
			if (other.trip != null)
				return false;
		} else if (!trip.equals(other.trip))
			return false;
		if (user == null) {
			if (other.user != null)
				return false;
		} else if (!user.equals(other.user))
			return false;
		return true;
	}

}
