package uo.sdi.model.types;

public enum SeatStatus {
	ACCEPTED,
	EXCLUDED
}
