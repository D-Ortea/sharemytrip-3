package uo.sdi.model.types;

public enum TripType {
	PROMOTOR,
	PENDIENTE,
	ADMITIDO,
	SIN_PLAZA,
	EXCLUIDO,
	CANCELADO
}
