package uo.sdi.model;

import uo.sdi.persistencia.util.Jpa;

public class Association {

	public static class Apuntar{
		
		public static void unlink(Application application){
			application.getUser()._getApplied().remove(application);
			application.getTrip()._getApplication().remove(application);
			
			Jpa.getManager().remove(application);
		}
	}
	
	public static class Sentar{
		
		public static void unlink(Seat seat){
			Trip trip = seat.getTrip();			
			trip.setAvailablePax(trip.getAvailablePax() + 1);
			
			trip._getSeats().remove(seat);
			seat.getUser()._getSeats().remove(seat);
			
			Jpa.getManager().remove(seat);
		}

		public static void link(User traveler, Trip trip) {
			Seat seat = new Seat(traveler,trip);
			
			traveler._getSeats().add(seat);
			trip._getSeats().add(seat);
			
			trip.setAvailablePax(trip.getAvailablePax() - 1);
		}
	}	
	
	
}
