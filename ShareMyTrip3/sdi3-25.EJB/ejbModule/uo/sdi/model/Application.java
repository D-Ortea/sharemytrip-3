package uo.sdi.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@IdClass(ApplicationKey.class)
@Table(name = "TAPPLICATIONS")
public class Application implements Serializable{

	private static final long serialVersionUID = 4820985707112458323L;
	
	@Id
	@ManyToOne
	@JoinColumn(name = "APPLICANTS_ID")
	private User user;
	@Id
	@ManyToOne
	@JoinColumn(name = "APPLIEDTRIPS_ID")
	private Trip trip;

	public Application() {
	}

	public Application(User user, Trip trip) {
		this.user = user;
		this.trip = trip;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Trip getTrip() {
		return trip;
	}

	public void setTrip(Trip trip) {
		this.trip = trip;
	}

	@Override
	public String toString() {
		return "Application [userId=" + user + ", tripId=" + trip + "]";
	}

	public Long[] makeKey() {
		return new Long[] { user.getId(), trip.getId() };
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((trip == null) ? 0 : trip.hashCode());
		result = prime * result + ((user == null) ? 0 : user.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Application other = (Application) obj;
		if (trip == null) {
			if (other.trip != null)
				return false;
		} else if (!trip.equals(other.trip))
			return false;
		if (user == null) {
			if (other.user != null)
				return false;
		} else if (!user.equals(other.user))
			return false;
		return true;
	}
}
