package uo.sdi.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import uo.sdi.model.types.SeatStatus;
import uo.sdi.model.types.TripStatus;
import uo.sdi.model.types.TripType;
import uo.sdi.model.types.UserStatus;

@Entity
@Table(name = "TUSERS")
@XmlRootElement(name = "usuario")
@XmlType
public class User implements Serializable {

	private static final long serialVersionUID = -2622160527029906699L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	private Long id;

	private String login;
	private String password;
	private String name;
	private String surname;
	private String email;

	@Enumerated(EnumType.ORDINAL)
	private UserStatus status;
	
	/* Propiedades calculadas */
	@Transient
	private List<Trip> viajesPendiente = null;
	@Transient
	private List<Trip> viajesAdmitido = null;
	@Transient
	private List<Trip> viajesExcluido = null;
	@Transient
	private List<Trip> viajesSinPlaza = null;
	@Transient
	private List<Trip> viajesCancelados = null;
	
	@Transient
	private int viajesPromovidos;
	@Transient
	private int viajesParticipante;
	
	@XmlTransient
	public List<Trip> getViajesPendiente() {
		return viajesPendiente;
	}

	@XmlTransient
	public List<Trip> getViajesAdmitido() {
		return viajesAdmitido;
	}

	@XmlTransient
	public List<Trip> getViajesExcluido() {
		return viajesExcluido;
	}

	@XmlTransient
	public List<Trip> getViajesSinPlaza() {
		return viajesSinPlaza;
	}
	
	@XmlTransient
	public List<Trip> getViajesCancelados() {
		return viajesCancelados;
	}
	
	@XmlTransient
	public int getViajesPromovidos() {
		return viajesPromovidos;
	}
	
	@XmlTransient
	public int getViajesParticipante() {
		return viajesParticipante;
	}
	
	/* Cross Reference */
	@OneToMany(mappedBy = "promoter")
    private Set<Trip> promoted = new HashSet<Trip>();
	@OneToMany(mappedBy="user")
	private Set<Seat> seats = new HashSet<Seat>();
	@OneToMany(mappedBy="user")
	private Set<Application> applied = new HashSet<Application>();

	@XmlTransient
	public Set<Trip> getPromoted() {
		return new HashSet<Trip>(promoted);
	}

	@XmlTransient
	public Set<Seat> getSeats() {
		return new HashSet<Seat>(seats);
	}
	
	protected Set<Seat> _getSeats() {
		return seats;
	}

	@XmlTransient
	public Set<Application> getApplied() {
		return new HashSet<Application>(applied);
	}
	
	protected Set<Application> _getApplied() {
		return applied;
	}

	/* Constructors */
	public User() {
	}

	public User(String login) {
		super();
		this.login = login;
	}

	public User(String login, String password, String name, String surname,
			String email, UserStatus status) {
		super();
		this.login = login;
		this.password = password;
		this.name = name;
		this.surname = surname;
		this.email = email;
		this.status = status;
	}

	/* Data Access */
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@XmlTransient
	public UserStatus getStatus() {
		return status;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	@XmlTransient
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}
	
	/* TIPOS DE VIAJES */
	// PROMOTOR ->  viajes que crea un usuario
	//              los que esten en el hashMap "promoted"
	//              Se accede directamente al hash. Se puede modificar al ser copia
	//              Lanza Lazy initialization (!)
	//
	// PENDIENTE -> el usuario esta en TAPPLICATION a la espera
	//              NO esta en TSEATS
	//              llamar a getViajesPendiente()
	//              No lanza lazy a no ser que se busquen coordenadas del trip
	//              
	// ADMITIDO  -> el usuario esta OK en TAPPLICATION y TSEAT
	//              llamar a getViajesAdmitido()
	//              No lanza lazy a no ser que se busquen coordenadas del trip
	//
	// SIN_PLAZA -> el usuario esta en TAPPLICATION pero NO en TSEAT
	//              ademas, en TSEAT el numero de admitted es igual a maxPax del viaje
	//              llamar a getViajesSinPlaza()
	//              No lanza lazy a no ser que se busquen coordenadas del trip
	//
	// EXCLUIDO->   el usuario esta EXPULSADO en TSEAT, aparece en TAPPLICATION tambien
	//              llamar a getViajesExcluido()
	//              No lanza lazy a no ser que se busquen coordenadas del trip
	
	/* Domain Methods */
	public void calcularViajes() {
		
		for (Trip trip : promoted) {
			if(trip.getStatus().equals(TripStatus.CANCELLED))
				trip.setType(TripType.CANCELADO);
			else
				trip.setType(TripType.PROMOTOR);
		}
		
		//Inicializar las listas
		viajesAdmitido = new ArrayList<Trip>();
		viajesExcluido = new ArrayList<Trip>();
		viajesPendiente = new ArrayList<Trip>();
		viajesSinPlaza = new ArrayList<Trip>();
		viajesCancelados = new ArrayList<Trip>();
		
		//Primero iteramos los viajes apuntados
		Trip trip;
		for (Application application : applied) {
			trip = application.getTrip();
			
			if(trip.getStatus().equals(TripStatus.CANCELLED)){
				trip.setType(TripType.CANCELADO);
				viajesCancelados.add(trip);
				continue;
			}
			
			//Contrastamos con los seats de cada viaje
			Set<Seat> tripSeats = trip.getSeats();
			int admittedCount = 0;
			boolean inSeats = false;
			for (Seat seat : tripSeats) {
				if(seat.getUser().equals(this)) {
					//Hemos encontrado una entrada válida, solo puede ser excluido o aceptado
					if(seat.getStatus().equals(SeatStatus.ACCEPTED)) {
						//Si esta aceptado, estamos admitidos
						viajesAdmitido.add(trip);
						trip.setType(TripType.ADMITIDO);
						inSeats = true;
						continue;
					} else {
						//Si no, estamos excluidos
						viajesExcluido.add(trip);
						trip.setType(TripType.EXCLUIDO);
						inSeats = true;
						continue;
					}
				}
				admittedCount = seat.getStatus().equals(SeatStatus.ACCEPTED) ? admittedCount+=1: admittedCount;
			}
			if(!inSeats) {
				if(admittedCount != trip.getMaxPax() || trip.getAvailablePax() != 0) {
					//Si no se ha cubierto el cupo
					viajesPendiente.add(trip);
					trip.setType(TripType.PENDIENTE);
				} else {
					// si ya estan todas las plazas
					trip.setType(TripType.SIN_PLAZA);
					viajesSinPlaza.add(trip);
				}
			}
		}
	}
	
	@XmlTransient
	public List<Trip> getAllViajes() {
		calcularViajes();
		
		List<Trip> allViajes = new ArrayList<Trip>();
		
		allViajes.addAll(promoted);
		allViajes.addAll(viajesAdmitido);
		allViajes.addAll(viajesPendiente);
		allViajes.addAll(viajesExcluido);
		allViajes.addAll(viajesSinPlaza);
		allViajes.addAll(viajesCancelados);
		
		return allViajes;
	}
	
	public void calculateStats(){
		this.viajesPromovidos = promoted.size();
		this.viajesParticipante = 0;
		for (Seat seat : seats) {
			if(seat.getStatus().equals(SeatStatus.ACCEPTED))
				viajesParticipante++;
		}
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", login=" + login + ", password=" + password
				+ ", name=" + name + ", surname=" + surname + ", status="
				+ ", email=" + email + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((login == null) ? 0 : login.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		if (login == null) {
			if (other.login != null)
				return false;
		} else if (!login.equals(other.login))
			return false;
		return true;
	}

	public void setStatus(UserStatus status) {
		this.status = status;
	}

}
