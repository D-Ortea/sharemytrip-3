package uo.sdi.business;

import alb.util.log.Log;

public class AccionExecutor {

	public <T> T execute(Accion<T> accion) throws Exception {
		try {
			return accion.execute();
		} catch(Exception e){
			Log.error(e);
			throw e;
		}
	}
}
