package uo.sdi.business;

import java.util.List;
import java.util.Map;

import uo.sdi.model.Trip;
import uo.sdi.model.User;

public interface TripService {

	public List<Trip> listadoPublico() throws Exception;
	public List<Trip> listadoUsuario(User user) throws Exception;
	public List<Trip> listadoPrivado(User user) throws Exception;
	public void apuntarseViaje(User user, Trip trip) throws Exception;
	public void cancelarAsistencia(User user, Trip trip) throws Exception;
	public Map<String, User[]> listadoPasajeros(Trip trip) throws Exception;
	public Trip confirmarPasajero(Trip trip, User pasajero) throws Exception;
	public Trip confirmarPasajeroId(Long tripId, Long pasajeroId) throws Exception;
	public Trip expulsar(Trip trip, User pasajero)throws Exception;
	public void registroViaje(User user, Trip viaje)throws Exception;
	public List<Trip> listadoPromotor(User user)throws Exception;
	public void cancelarViaje(List<Trip> viajesSeleccionados) throws Exception;
	public void editarViaje(Trip viajePromo) throws Exception;
	public List<Trip> listActivePromoted(Long id) throws Exception;
	public List<User> listPasajerosSinConfirmar(Long id) throws Exception;
	public List<Trip> viajesConParticipacion(Long id) throws Exception;
}
