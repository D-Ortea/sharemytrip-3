package uo.sdi.business;

import uo.sdi.business.LoginService;

public interface ServicesFactory {

	LoginService getLoginService();
	TripService getTripService();
	UserService getUserService();
	RatingService getRatingService();
}
