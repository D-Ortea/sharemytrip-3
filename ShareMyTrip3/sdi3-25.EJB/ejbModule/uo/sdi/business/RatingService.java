package uo.sdi.business;

import java.util.List;

import uo.sdi.model.Rating;

public interface RatingService {
	List<Rating> getRecentRatings() throws Exception;
	boolean borrarComentario(Long id) throws Exception;
}
