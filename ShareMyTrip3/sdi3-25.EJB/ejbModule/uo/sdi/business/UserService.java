package uo.sdi.business;

import java.util.List;

import uo.sdi.model.User;

public interface UserService {
	void saveUser(User user) throws Exception;
	boolean isLoginAvailable(String login) throws Exception;
	List<User> listUsers() throws Exception;
	List<User> listActiveUsers() throws Exception;
	boolean deactivateUser(String login) throws Exception;
	int getViajesPromovidos(User user);
	int getViajesParticipante(User user);
}
