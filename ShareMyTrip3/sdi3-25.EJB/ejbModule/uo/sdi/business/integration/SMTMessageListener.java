package uo.sdi.business.integration;

import java.util.List;

import javax.annotation.Resource;
import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.MessageProducer;
import javax.jms.Session;

import uo.sdi.infraestructura.Factories;
import uo.sdi.model.Trip;
import alb.util.log.Log;

@MessageDriven(activationConfig = { 
		@ActivationConfigProperty(propertyName = "destination",
				propertyValue = "java:jboss/exported/jms/queue/smt-inputChannel") })
public class SMTMessageListener implements MessageListener {

	@Resource(mappedName = "java:/ConnectionFactory")
	private ConnectionFactory factory;

	@Resource(mappedName = "java:jboss/exported/jms/topic/smt-subscription")
	private Destination queue;
	
	@Resource(mappedName = "java:jboss/exported/jms/queue/smt-errorQueue")
	private Destination errorQueue;
	
	@Override
	public void onMessage(Message msg) {
		Log.info("Message recieved", "");
		try {
			process(msg);
		} catch (Exception jex) {
			Log.error(jex);
		}
	}

	private void process(Message msg) throws Exception {
		if (!(msg instanceof MapMessage)) {
			Log.error("Message not of expected type");
			return;
		}

		MapMessage mmsg = (MapMessage) msg;

		Long userId = mmsg.getLong("userId");
		Long tripId = mmsg.getLong("tripId");
		
		if(userId == null || tripId == null)
			sendMessage(mmsg,errorQueue);
		
		List<Trip> viajes = Factories.services.getTripService()
				.viajesConParticipacion(userId);

		for (Trip trip : viajes) {
			if (trip.getId().equals(tripId)) {
				sendMessage(mmsg,queue);
				return;
			}
		}

		//Si llegamos aqui, ha ocurrido un error de validacion
		sendMessage(mmsg,errorQueue);
	}

	private void sendMessage(MapMessage mmsg,Destination destination) throws JMSException {
		Connection con = null;

		con = factory.createConnection("sdi", "password");
		Session session = con.createSession(false, Session.AUTO_ACKNOWLEDGE);
		MessageProducer prodyuusa = session.createProducer(destination);
		prodyuusa.send(mmsg);
		
		if(destination.equals(queue))
			Log.debug("Mensaje reenviado a [%s]", "Share My Trip Topic");
		else	
			Log.warn("Mensaje reenviado a [%s]", "Error Queue");

		con.close();
	}
}
