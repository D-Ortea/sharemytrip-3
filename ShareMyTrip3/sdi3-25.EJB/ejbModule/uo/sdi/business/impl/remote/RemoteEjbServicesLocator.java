package uo.sdi.business.impl.remote;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import uo.sdi.business.LoginService;
import uo.sdi.business.RatingService;
import uo.sdi.business.ServicesFactory;
import uo.sdi.business.TripService;
import uo.sdi.business.UserService;
import alb.util.log.Log;

public class RemoteEjbServicesLocator implements ServicesFactory{

	private static final String LOGIN_SERVICE_JNDI_KEY =
					  "sdi3-25/"
					+ "sdi3-25.EJB/"
					+ "EjbLoginServiceImpl!"
					+ "uo.sdi.business.impl.remote.RemoteLoginService";
	
	private static final String TRIP_SERVICE_JNDI_KEY =
					  "sdi3-25/"
					+ "sdi3-25.EJB/"
					+ "EjbTripServiceImpl!"
					+ "uo.sdi.business.impl.remote.RemoteTripService";
	
	private static final String  USER_SERVICE_JNDI_KEY = 
					  "sdi3-25/"
					+ "sdi3-25.EJB/"
					+ "EjbUserServiceImpl!"
					+ "uo.sdi.business.impl.remote.RemoteUserService";
	private static final String  RATING_SERVICE_JNDI_KEY = 
		      "sdi3-25/"
			+ "sdi3-25.EJB/"
			+ "EjbRatingServiceImpl!"
			+ "uo.sdi.business.impl.remote.RemoteRatingService";
	
	@Override
	public LoginService getLoginService() {
		Log.info("Usando Servicio remoto de busqueda de LoginService","");
		try {
			Context ctx = new InitialContext();
			return (LoginService) ctx.lookup(LOGIN_SERVICE_JNDI_KEY);
		} catch (NamingException e) {
			Log.error(e);
			throw new RuntimeException("JNDI problem", e);
		}
	}

	@Override
	public TripService getTripService() {
		Log.info("Usando Servicio remoto de busqueda de TripService","");
		try {
			Context ctx = new InitialContext();
			return (TripService) ctx.lookup(TRIP_SERVICE_JNDI_KEY);
		} catch (NamingException e) {
			Log.error(e);
			throw new RuntimeException("JNDI problem", e);
		}
	}

	@Override
	public UserService getUserService() {
		Log.info("Usando Servicio remoto de busqueda de UserService","");
		try {
			Context ctx = new InitialContext();
			return (UserService) ctx.lookup(USER_SERVICE_JNDI_KEY);
		} catch (NamingException e) {
			Log.error(e);
			throw new RuntimeException("JNDI problem", e);
		}
	}

	@Override
	public RatingService getRatingService() {
		Log.info("Usando Servicio remoto de busqueda de RatingService","");
		try {
			Context ctx = new InitialContext();
			return (RatingService) ctx.lookup(RATING_SERVICE_JNDI_KEY);
		} catch (NamingException e) {
			Log.error(e);
			throw new RuntimeException("JNDI problem", e);
		}
	}
}
