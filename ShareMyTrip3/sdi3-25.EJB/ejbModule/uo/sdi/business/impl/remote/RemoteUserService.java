package uo.sdi.business.impl.remote;

import javax.ejb.Remote;

import uo.sdi.business.UserService;

@Remote
public interface RemoteUserService extends UserService{

}
