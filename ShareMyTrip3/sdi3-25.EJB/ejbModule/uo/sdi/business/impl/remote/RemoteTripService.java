package uo.sdi.business.impl.remote;

import javax.ejb.Remote;

import uo.sdi.business.TripService;

@Remote
public interface RemoteTripService extends TripService{

}
