package uo.sdi.business.impl.remote;

import javax.ejb.Remote;

import uo.sdi.business.RatingService;

@Remote
public interface RemoteRatingService extends RatingService{

}
