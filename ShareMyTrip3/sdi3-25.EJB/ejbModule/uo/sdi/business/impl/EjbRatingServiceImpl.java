package uo.sdi.business.impl;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.jws.WebMethod;
import javax.jws.WebService;

import uo.sdi.business.AccionExecutor;
import uo.sdi.business.impl.classes.BorrarComentario;
import uo.sdi.business.impl.classes.ListarRatingsRecientes;
import uo.sdi.business.impl.local.LocalRatingService;
import uo.sdi.business.impl.remote.RemoteRatingService;
import uo.sdi.infraestructura.Factories;
import uo.sdi.model.Rating;

@Stateless
@WebService(name="RatingService")
public class EjbRatingServiceImpl implements RemoteRatingService,  LocalRatingService{

	private AccionExecutor executor = new AccionExecutor();
	
	@Override
	public List<Rating> getRecentRatings() throws Exception {
		return executor.execute(new ListarRatingsRecientes());
	}

	@Override
	public boolean borrarComentario(Long id) throws Exception {
		return executor.execute(new BorrarComentario(id));		
	}

	@WebMethod(action="datosRating")
	public List<String> getDatosRating(Rating r) throws Exception {		
		Rating rating = Factories.persistence.getRatingFinder().findRating(r.getId());
		List<String> datos = new ArrayList<>();
		datos.add(rating.getAbout().getTrip().getDestination().toString());
		datos.add(rating.getFrom().getUser().toString());
		datos.add(rating.getAbout().getUser().toString());
		return datos;
	}
	
}
