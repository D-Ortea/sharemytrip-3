package uo.sdi.business.impl.local;

import javax.ejb.Local;

import uo.sdi.business.RatingService;

@Local
public interface LocalRatingService extends RatingService{

}
