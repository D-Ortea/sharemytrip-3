package uo.sdi.business.impl.local;

import javax.ejb.Local;

import uo.sdi.business.TripService;

@Local
public interface LocalTripService extends TripService{

}
