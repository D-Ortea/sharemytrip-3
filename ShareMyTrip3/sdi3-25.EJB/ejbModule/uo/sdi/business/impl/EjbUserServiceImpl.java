package uo.sdi.business.impl;

import java.util.List;
import java.util.Set;

import javax.ejb.Stateless;
import javax.jws.WebMethod;
import javax.jws.WebService;

import uo.sdi.business.AccionExecutor;
import uo.sdi.business.impl.classes.DeactivateUser;
import uo.sdi.business.impl.classes.ListarUsuarios;
import uo.sdi.business.impl.classes.ListarUsuariosActivos;
import uo.sdi.business.impl.classes.LoginAvailable;
import uo.sdi.business.impl.classes.UsuarioAlta;
import uo.sdi.business.impl.local.LocalUserService;
import uo.sdi.business.impl.remote.RemoteUserService;
import uo.sdi.infraestructura.Factories;
import uo.sdi.model.Seat;
import uo.sdi.model.User;
import uo.sdi.model.types.SeatStatus;

@Stateless
@WebService(name="UserService")
public class EjbUserServiceImpl implements RemoteUserService,LocalUserService  {

	private AccionExecutor executor = new AccionExecutor();

	@Override
	public void saveUser(User user) throws Exception {
		executor.execute(new UsuarioAlta(user));
	}

	@Override
	public boolean isLoginAvailable(String login) throws Exception {
		return executor.execute(new LoginAvailable(login));
	}
	
	@Override
	public List<User> listUsers() throws Exception {
		return executor.execute(new ListarUsuarios());
	}

	@Override
	public List<User> listActiveUsers() throws Exception {
		return executor.execute(new ListarUsuariosActivos());
	}

	@Override
	public boolean deactivateUser(String login) throws Exception {
		return executor.execute(new DeactivateUser(login));
	}

	@Override
	public int getViajesParticipante(User user) {
		User u = Factories.persistence.getUserFinder().findByID(user.getId());
		Set<Seat> seats = u.getSeats();
		int counter = 0;
		for (Seat seat : seats) {
			if(seat.getStatus().equals(SeatStatus.ACCEPTED))
				counter++;
		}
		
		return counter;
	}

	@Override
	public int getViajesPromovidos(User user) {
		User u = Factories.persistence.getUserFinder().findByID(user.getId());
		return u.getPromoted().size();
	}

}
