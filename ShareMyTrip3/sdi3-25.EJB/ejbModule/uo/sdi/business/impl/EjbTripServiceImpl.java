package uo.sdi.business.impl;

import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.jws.WebService;

import uo.sdi.business.AccionExecutor;
import uo.sdi.business.impl.classes.ApuntarseViaje;
import uo.sdi.business.impl.classes.CancelarAsistencia;
import uo.sdi.business.impl.classes.CancelarViaje;
import uo.sdi.business.impl.classes.ConfirmarPasajero;
import uo.sdi.business.impl.classes.EditarViaje;
import uo.sdi.business.impl.classes.Expulsar;
import uo.sdi.business.impl.classes.ListadoPasajeros;
import uo.sdi.business.impl.classes.ListadoPrivado;
import uo.sdi.business.impl.classes.ListadoPromotor;
import uo.sdi.business.impl.classes.ListadoPublico;
import uo.sdi.business.impl.classes.ListadoUsuario;
import uo.sdi.business.impl.classes.ParticipacionActiva;
import uo.sdi.business.impl.classes.PasajerosSinConfirmar;
import uo.sdi.business.impl.classes.RegistrarViaje;
import uo.sdi.business.impl.classes.ViajesPromocionadosActive;
import uo.sdi.business.impl.local.LocalTripService;
import uo.sdi.business.impl.remote.RemoteTripService;
import uo.sdi.model.Trip;
import uo.sdi.model.User;

@Stateless
@WebService(name="TripService")
public class EjbTripServiceImpl implements RemoteTripService, LocalTripService {

	private AccionExecutor executor = new AccionExecutor();

	@Override
	public List<Trip> listadoPublico() throws Exception {
		return executor.execute(new ListadoPublico());
	}

	@Override
	public List<Trip> listadoUsuario(User user) throws Exception {
		return executor.execute(new ListadoUsuario(user));
	}

	@Override
	public List<Trip> listadoPrivado(User user) throws Exception {
		return executor.execute(new ListadoPrivado(user));
	}

	@Override
	public void apuntarseViaje(User user, Trip trip) throws Exception {
		executor.execute(new ApuntarseViaje(user, trip));
	}

	@Override
	public void cancelarAsistencia(User user, Trip trip) throws Exception {
		executor.execute(new CancelarAsistencia(user, trip));
	}

	@Override
	public Map<String, User[]> listadoPasajeros(Trip trip) throws Exception {
		return executor.execute(new ListadoPasajeros(trip));
	}

	@Override
	public Trip confirmarPasajero(Trip trip, User pasajero)
			throws Exception
	{
		return executor.execute(new ConfirmarPasajero(trip, pasajero));
	}

	@Override
	public Trip expulsar(Trip trip, User pasajero) throws Exception {
		return executor.execute(new Expulsar(trip,pasajero));
	}

	@Override
	public void registroViaje(User user, Trip viaje)
			throws Exception
	{	
		executor.execute(new RegistrarViaje(user,viaje));
	}

	@Override
	public List<Trip> listadoPromotor(User user) throws Exception {
		return executor.execute(new ListadoPromotor(user));
	}

	@Override
	public void cancelarViaje(List<Trip> trips) throws Exception {
		executor.execute(new CancelarViaje(trips));
	}

	@Override
	public void editarViaje(Trip viajePromo) throws Exception {
		executor.execute(new EditarViaje(viajePromo));
	}

	@Override
	public List<Trip> listActivePromoted(Long id) throws Exception {
		return executor.execute(new ViajesPromocionadosActive(id));
	}

	@Override
	public Trip confirmarPasajeroId(Long tripId, Long pasajeroId)
			throws Exception
	{
		return executor.execute(new ConfirmarPasajero(tripId, pasajeroId));
	}

	@Override
	public List<User> listPasajerosSinConfirmar(Long id) throws Exception {
		return executor.execute(new PasajerosSinConfirmar(id));
	}

	@Override
	public List<Trip> viajesConParticipacion(Long id) throws Exception {
		return executor.execute(new ParticipacionActiva(id));
	}
}
