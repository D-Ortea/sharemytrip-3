package uo.sdi.business.impl.classes;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import alb.util.log.Log;
import uo.sdi.business.Accion;
import uo.sdi.model.Trip;
import uo.sdi.model.User;
import uo.sdi.persistencia.util.Jpa;

public class ListadoPromotor implements Accion<List<Trip>> {

	private User user;
	
	public ListadoPromotor(User user) {
		this.user = user;
	}

	@Override
	public List<Trip> execute() throws Exception {
		user = Jpa.getManager().merge(user);
		
		Set<Trip> promoted = user.getPromoted();
		
		List<Trip> list = new ArrayList<Trip>();
		
		for (Trip trip : promoted) {
			if(trip.isNotOver())
				list.add(trip);
		}
		
		Log.debug("Obtenida lista de viajes de promotor conteniendo [%s] viajes del usuario [%s]",
				list.size(), user.getId());
		
		return list;
	}

}
