package uo.sdi.business.impl.classes;

import java.util.List;

import alb.util.log.Log;
import uo.sdi.business.Accion;
import uo.sdi.model.Trip;
import uo.sdi.model.types.TripStatus;
import uo.sdi.persistencia.util.Jpa;

public class CancelarViaje implements Accion<Trip> {

	private List<Trip> trips;

	public CancelarViaje(List<Trip> trips) {
		this.trips = trips;
	}

	@Override
	public Trip execute() throws Exception {
		Trip trip = null;
		for (Trip tripSelected : trips) {
			trip = Jpa.getManager().merge(tripSelected);
			
			if(!trip.isNotOver())
				throw new RuntimeException();
			trip.setStatus(TripStatus.CANCELLED);
			Log.info("Se ha cancelado el viaje [%s]", trip.getId());
			
		}
		
		return trip;
	}

}
