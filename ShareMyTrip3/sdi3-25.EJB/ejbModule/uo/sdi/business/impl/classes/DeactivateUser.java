package uo.sdi.business.impl.classes;

import java.util.List;

import uo.sdi.business.Accion;
import uo.sdi.infraestructura.Factories;
import uo.sdi.model.Seat;
import uo.sdi.model.Trip;
import uo.sdi.model.User;
import uo.sdi.model.types.SeatStatus;
import uo.sdi.model.types.TripStatus;
import uo.sdi.model.types.UserStatus;

public class DeactivateUser implements Accion<Boolean> {

	private String login;

	public DeactivateUser(String login) {
		this.login = login;	
	}

	@Override
	public Boolean execute() throws Exception {
		List<User> users =  Factories.persistence.getUserFinder().findByLogin(login);
		if(users.isEmpty())
			return false;
		
		User user = users.get(0);
		
		if(user.getStatus().equals(UserStatus.CANCELLED))
			return false;
		
		for (Trip trip : user.getPromoted()) {
			trip.setStatus(TripStatus.CANCELLED);
		}
		
		for (Seat seat : user.getSeats()) {
			seat.setStatus(SeatStatus.EXCLUDED);
		}
		
		user.setStatus(UserStatus.CANCELLED);
		
		return true;
	}

}
