package uo.sdi.business.impl.classes;

import java.util.List;

import alb.util.log.Log;
import uo.sdi.business.Accion;
import uo.sdi.infraestructura.Factories;
import uo.sdi.model.Application;
import uo.sdi.model.Seat;
import uo.sdi.model.Trip;
import uo.sdi.model.User;
import uo.sdi.persistencia.util.Jpa;

public class CancelarAsistencia implements Accion<Trip> {

	private User user;
	private Trip trip;

	public CancelarAsistencia(User user, Trip trip) {
		this.user = user;
		this.trip = trip;	
	}

	@Override
	public Trip execute() {
		
		trip = Jpa.getManager().merge(trip);
	
		user = Jpa.getManager().merge(user);
		
		List<Application> applicationList = Factories.persistence
				.getApplicationFinder().findByKey(user, trip);
		
		Application app = applicationList.get(0);
		
		Jpa.getManager().remove(app);
		
		List<Seat> seatList = Factories.persistence.getSeatFinder()
				.findByKey(user, trip);
		
		if(seatList.size() != 0){
			Jpa.getManager().remove(seatList.get(0));
			trip.setAvailablePax(trip.getAvailablePax() + 1);
		}
		
		if(!trip.disponible())
			throw new RuntimeException();
		
		Log.info("El usuario [%s] se ha borrado del viaje [%s]"
				, user.getId(),trip.getId());
		
		return null;
	}

}
