package uo.sdi.business.impl.classes;

import alb.util.log.Log;
import uo.sdi.business.Accion;
import uo.sdi.infraestructura.Factories;
import uo.sdi.model.Application;
import uo.sdi.model.Trip;
import uo.sdi.model.User;
import uo.sdi.persistencia.util.Jpa;

public class ApuntarseViaje implements Accion<Application> {

	private User user;
	private Trip trip;

	public ApuntarseViaje(User user, Trip trip) {
		this.user = user;
		this.trip = trip;
	}

	@Override
	public Application execute() {

		user = Jpa.getManager().merge(user);

		trip = Factories.persistence.getTripFinder().findByID(trip.getId());

		Application a = new Application(user, trip);
		Jpa.getManager().persist(a);

		if (!trip.isNotOver())
			throw new RuntimeException();

		Log.info("Se ha insertado el usuario [%s] en el viaje [%s]",
				user.getName(), trip.getId());

		return a;
	}

}
