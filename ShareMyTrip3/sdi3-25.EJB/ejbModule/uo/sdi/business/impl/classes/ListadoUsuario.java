package uo.sdi.business.impl.classes;

import java.util.List;

import alb.util.log.Log;
import uo.sdi.business.Accion;
import uo.sdi.model.Trip;
import uo.sdi.model.User;
import uo.sdi.persistencia.util.Jpa;

public class ListadoUsuario implements Accion<List<Trip>> {

	private User user;
	
	public ListadoUsuario(User user) {
		this.user = user;	
	}

	@Override
	public List<Trip> execute() {
		user = Jpa.getManager().merge(user);
		
		List<Trip> result = user.getAllViajes();
				
		Log.info("Se han recuperado todos los viajes del usuario [%s], total [%s]",
				user.getId(),result.size());
		
		return result;
	}

}
