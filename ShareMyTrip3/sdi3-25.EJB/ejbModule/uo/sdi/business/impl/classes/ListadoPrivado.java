package uo.sdi.business.impl.classes;

import java.util.ArrayList;
import java.util.List;

import alb.util.log.Log;
import uo.sdi.business.Accion;
import uo.sdi.infraestructura.Factories;
import uo.sdi.model.Application;
import uo.sdi.model.Trip;
import uo.sdi.model.User;
import uo.sdi.persistencia.util.Jpa;

public class ListadoPrivado implements Accion<List<Trip>> {

	private User user;

	public ListadoPrivado(User user) {
		this.user = user;
	}

	@Override
	public List<Trip> execute() {
		user = Jpa.getManager().merge(user);
		
		List<Trip> allViajes;
		List<Trip> viajes = new ArrayList<Trip>();
		
		allViajes = Factories.persistence.getTripFinder().findAll();
		for (Trip trip : allViajes) {
			trip.getPromoter();
			if (trip.disponible()) {
				boolean alreadyApplied = false;
				for (Application application : trip.getApplication()) {
					if (application.getUser().equals(user)) {
						alreadyApplied = true;
						break;
					}
				}
				if (!alreadyApplied) {
					viajes.add(trip);
				}
			}
		}
		
		viajes.removeAll(user.getPromoted());

		Log.debug("Obtenida lista de viajes conteniendo [%d] viajes",
				viajes.size());

		return viajes;
	}

}
