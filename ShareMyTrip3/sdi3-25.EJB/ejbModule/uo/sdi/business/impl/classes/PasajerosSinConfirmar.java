package uo.sdi.business.impl.classes;

import java.util.ArrayList;
import java.util.List;

import uo.sdi.business.Accion;
import uo.sdi.infraestructura.Factories;
import uo.sdi.model.Application;
import uo.sdi.model.Seat;
import uo.sdi.model.Trip;
import uo.sdi.model.User;
import uo.sdi.model.types.SeatStatus;
import uo.sdi.model.types.UserStatus;

public class PasajerosSinConfirmar implements Accion<List<User>> {

	private Long id;

	public PasajerosSinConfirmar(Long id) {
		this.id = id;
	}

	@Override
	public List<User> execute() throws Exception {
		Trip trip = Factories.persistence.getTripFinder().findByID(id);

		List<User> appliedUsers = new ArrayList<>();
		List<User> seatUsers = new ArrayList<>();

		for (Seat s : trip.getSeats()) {
			if (s.getUser().getStatus().equals(UserStatus.ACTIVE)) {
				if (s.getStatus().equals(SeatStatus.EXCLUDED)) {
					appliedUsers.add(s.getUser());
					continue;
				} else {
					seatUsers.add(s.getUser());
				}
			}
		}
		for (Application a : trip.getApplication()) {
			User u = a.getUser();
			if (u.getStatus().equals(UserStatus.ACTIVE)) {
				if (!appliedUsers.contains(u))
					appliedUsers.add(a.getUser());
			}
		}

		appliedUsers.removeAll(seatUsers);

		return appliedUsers;
	}

}
