package uo.sdi.business.impl.classes;

import alb.util.log.Log;
import uo.sdi.business.Accion;
import uo.sdi.infraestructura.Factories;
import uo.sdi.model.Rating;
import uo.sdi.persistencia.util.Jpa;

public class BorrarComentario implements Accion<Boolean> {

	private Long id;
	
	public BorrarComentario(Long id) {
		this.id = id;
	}

	@Override
	public Boolean execute() throws Exception {
		Rating rating = Factories.persistence
				.getRatingFinder().findRating(id);
		
		if(rating == null) {
			return false;
		}
		Jpa.getManager().remove(rating);		

		Log.info("El rating [%s] se ha borrado", rating.getId());
		
		return true;
	}

}
