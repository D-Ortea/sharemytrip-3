package uo.sdi.business.impl.classes;

import java.util.ArrayList;
import java.util.List;

import uo.sdi.business.Accion;
import uo.sdi.infraestructura.Factories;
import uo.sdi.model.Seat;
import uo.sdi.model.Trip;
import uo.sdi.model.User;
import uo.sdi.model.types.SeatStatus;
import uo.sdi.model.types.UserStatus;

public class ParticipacionActiva implements Accion<List<Trip>> {

	private Long id;

	public ParticipacionActiva(Long id) {
		this.id = id;
	}

	@Override
	public List<Trip> execute() throws Exception {
		User user = Factories.persistence.getUserFinder().findByID(id);
		
		List<Trip> result = new ArrayList<Trip>();
		
		if(user.getStatus().equals(UserStatus.CANCELLED))
			return result;
	
		result.addAll(user.getPromoted());
		
		for (Seat seat : user.getSeats()) {
			if(seat.getStatus().equals(SeatStatus.ACCEPTED))
				result.add(seat.getTrip());
		}
		
		return result;
	}

}
