package uo.sdi.business.impl.classes;

import uo.sdi.business.Accion;
import uo.sdi.model.Trip;
import uo.sdi.model.User;
import uo.sdi.model.types.TripStatus;
import uo.sdi.persistencia.util.Jpa;

public class RegistrarViaje implements Accion<Trip> {

	private User user;
	private Trip viaje;

	public RegistrarViaje(User user, Trip viaje) {
		this.user = user;
		this.viaje = viaje;
	}

	@Override
	public Trip execute() throws Exception {
		user = Jpa.getManager().merge(user);
		
		viaje = Jpa.getManager().merge(viaje);
		
		Trip trip = new Trip(viaje.getDeparture(),
				viaje.getDestination(),
				viaje.getArrivalDate(), viaje.getDepartureDate(),
				viaje.getClosingDate(), viaje.getAvailablePax(),
				viaje.getMaxPax(), viaje.getEstimatedCost(),
				viaje.getComments(), TripStatus.OPEN, user);
		
		Jpa.getManager().persist(trip);
		
		return trip;
	}
}
