package uo.sdi.business.impl.classes;

import java.util.List;

import alb.util.log.Log;
import uo.sdi.business.Accion;
import uo.sdi.infraestructura.Factories;
import uo.sdi.model.User;

public class LoginAvailable implements Accion<Boolean> {

	private String login;

	public LoginAvailable(String login) {
		this.login = login;
	}

	@Override
	public Boolean execute() throws Exception {

		List<User> users = Factories.persistence.getUserFinder().findByLogin(login);
		
		if(users.size() == 0) {
			Log.debug("El login [%s] esta disponible", login);
			return true;
		} else {
			Log.debug("El login [%s] NO esta disponible", login);
			return false;
		}
	}

}
