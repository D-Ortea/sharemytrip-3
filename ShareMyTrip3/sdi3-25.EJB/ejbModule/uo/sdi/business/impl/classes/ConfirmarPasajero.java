package uo.sdi.business.impl.classes;

import java.util.List;

import alb.util.log.Log;
import uo.sdi.business.Accion;
import uo.sdi.infraestructura.Factories;
import uo.sdi.model.Seat;
import uo.sdi.model.Trip;
import uo.sdi.model.User;
import uo.sdi.model.types.SeatStatus;
import uo.sdi.persistencia.util.Jpa;

public class ConfirmarPasajero implements Accion<Trip> {

	private Trip trip;
	private User pasajero;

	public ConfirmarPasajero(Trip trip, User pasajero) {
		this.trip = Jpa.getManager().merge(trip);
		this.pasajero = Jpa.getManager().merge(pasajero);
	}

	public ConfirmarPasajero(Long tripId, Long pasajeroId) {
		this.pasajero = Factories.persistence.getUserFinder().findByID(pasajeroId);
		this.trip = Factories.persistence.getTripFinder().findByID(tripId);
	}

	@Override
	public Trip execute() {
		if(trip.getAvailablePax() == 0){
			throw new RuntimeException();
		}
		
		List<Seat> seats = Factories.persistence.getSeatFinder().findByKey(pasajero, trip);
		
		if(seats.size() != 0){
			seats.get(0).setStatus(SeatStatus.ACCEPTED);
			trip.setAvailablePax(trip.getAvailablePax() - 1);
			Log.info("Al usuario [%s] se le ha aceptado finalmente la plaza en el viaje [%s]",
					pasajero.getId(),trip.getId());
		}
		else {
			Seat seat = new Seat(pasajero, trip);
		
			Jpa.getManager().persist(seat);
			Jpa.getManager().merge(trip);
			
			Log.info("Al usuario [%s] se le ha asignado una plaza en el viaje [%s]",
					pasajero.getId(),trip.getId());
		}
		
		if(!trip.isNotOver())
			throw new RuntimeException();
			
		return trip;
	}

}
