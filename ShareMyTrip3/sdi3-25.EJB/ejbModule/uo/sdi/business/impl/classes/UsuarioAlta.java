package uo.sdi.business.impl.classes;

import java.util.List;

import alb.util.log.Log;
import uo.sdi.business.Accion;
import uo.sdi.infraestructura.Factories;
import uo.sdi.model.User;
import uo.sdi.persistencia.util.Jpa;

public class UsuarioAlta implements Accion<User> {

	private User user;
	
	public UsuarioAlta(User user) {
		this.user = new User(user.getLogin(), user.getPassword(), user.getName(),
				user.getSurname(), user.getEmail(), user.getStatus());	
	}
	
	@Override
	public User execute() throws Exception{
		
		List<User> userlist = Factories.persistence.getUserFinder()
				.findByLogin(user.getLogin());
		
		if(userlist.size() != 0)
			throw new Exception("Ya existe un usuario con este nombre");
		
		Jpa.getManager().persist(user);
		
		Log.info("Se ha regitrado al usuario [%s] en el sistema"
					,user.getName());
		
		return user;
	}

}
