package uo.sdi.business.impl.classes;

import alb.util.log.Log;
import uo.sdi.business.Accion;
import uo.sdi.infraestructura.Factories;
import uo.sdi.model.Trip;

public class EditarViaje implements Accion<Trip> {

	private Trip viajePromo;

	public EditarViaje(Trip viajePromo) {
		this.viajePromo = viajePromo;
	}

	@Override
	public Trip execute() throws Exception {
		Long idLong = viajePromo.getId();
		Trip trip = Factories.persistence.getTripFinder().findByID(idLong);
		
		trip.setDeparture(viajePromo.getDeparture());
		trip.setDestination(viajePromo.getDestination());
		
		trip.setArrivalDate(viajePromo.getArrivalDate());
		trip.setClosingDate(viajePromo.getClosingDate());
		trip.setDepartureDate(viajePromo.getDepartureDate());
		
		trip.setAvailablePax(viajePromo.getAvailablePax());
		trip.setMaxPax(viajePromo.getMaxPax());
		
		trip.setComments(viajePromo.getComments());
		
		trip.setEstimatedCost(viajePromo.getEstimatedCost());
		
		Log.info("Viaje [%s] editado", idLong);
		
		return trip;
	}
}
