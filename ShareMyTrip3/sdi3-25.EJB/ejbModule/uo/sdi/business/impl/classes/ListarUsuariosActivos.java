package uo.sdi.business.impl.classes;

import java.util.List;

import uo.sdi.business.Accion;
import uo.sdi.infraestructura.Factories;
import uo.sdi.model.User;

public class ListarUsuariosActivos implements Accion<List<User>> {

	@Override
	public List<User> execute() throws Exception {
		List<User> users =  Factories.persistence.getUserFinder().findActivos();
		for (User user : users) {
			user.calculateStats();
		}
		return users;
	}

}
