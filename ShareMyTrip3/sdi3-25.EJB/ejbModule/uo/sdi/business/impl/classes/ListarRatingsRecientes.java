package uo.sdi.business.impl.classes;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import uo.sdi.business.Accion;
import uo.sdi.infraestructura.Factories;
import uo.sdi.model.Rating;


public class ListarRatingsRecientes implements Accion<List<Rating>> {

	@Override
	public List<Rating> execute() throws Exception {
		List<Rating> ratings = Factories.persistence.getRatingFinder().findAll();
		List<Rating> recentRatings = new ArrayList<>();
		
		for(Rating rating : ratings) {
			Date arrivalDate = rating.getFrom().getTrip().getArrivalDate();
			if(arrivalDate.after(oneMonthAgo())) {
				recentRatings.add(rating);
			}
		}
		
		return recentRatings;
	}
	
	private Date oneMonthAgo() {
		Calendar c = Calendar.getInstance();
		c.add(Calendar.MONTH, -1);
		return c.getTime();
	}

}
