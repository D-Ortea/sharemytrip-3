package uo.sdi.business.impl.classes;

import java.util.List;

import alb.util.log.Log;
import uo.sdi.business.Accion;
import uo.sdi.infraestructura.Factories;
import uo.sdi.model.Seat;
import uo.sdi.model.Trip;
import uo.sdi.model.User;
import uo.sdi.model.types.SeatStatus;
import uo.sdi.persistencia.util.Jpa;

public class Expulsar implements Accion<Trip> {

	private Trip trip;
	private User pasajero;

	public Expulsar(Trip trip, User pasajero) {
		this.trip = trip;
		this.pasajero = pasajero;
		
	}

	@Override
	public Trip execute() throws Exception {
		trip = Jpa.getManager().merge(trip);
		
		pasajero = Jpa.getManager().merge(pasajero);
		
		List<Seat> seatList = Factories.persistence.getSeatFinder().findByKey(pasajero, trip);
		
		seatList.get(0).setStatus(SeatStatus.EXCLUDED);
		
		trip.setAvailablePax(trip.getAvailablePax() + 1);
		
		Log.info("Se ha excluido al pasajero [%s] del viaje [%s]",pasajero.getId(),trip.getId());
		
		if(!trip.isNotOver())
			throw new RuntimeException();
		
		return trip;
	}

}
