package uo.sdi.business.impl.classes;

import java.util.ArrayList;
import java.util.List;

import uo.sdi.business.Accion;
import uo.sdi.infraestructura.Factories;
import uo.sdi.model.Trip;
import uo.sdi.model.User;

public class ViajesPromocionadosActive implements Accion<List<Trip>> {

	private Long id;

	public ViajesPromocionadosActive(Long id) {
		this.id = id;
	}

	@Override
	public List<Trip> execute() throws Exception {
		User user = Factories.persistence.getUserFinder().findByID(id);
		
		List<Trip> list = new ArrayList<>(user.getPromoted());
		
		List<Trip> result = new ArrayList<Trip>();
		
		for (Trip trip : list) {
			if(trip.isNotOver()){
				result.add(trip);
			}
		}
		
		return result;
	}

}
