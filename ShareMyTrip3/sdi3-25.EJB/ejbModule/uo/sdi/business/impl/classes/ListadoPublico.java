package uo.sdi.business.impl.classes;

import java.util.ArrayList;
import java.util.List;

import alb.util.log.Log;
import uo.sdi.business.Accion;
import uo.sdi.infraestructura.Factories;
import uo.sdi.model.Trip;

public class ListadoPublico implements Accion<List<Trip>> {

	@Override
	public List<Trip> execute() {
		
		List<Trip> allViajes;
		List<Trip> viajes = new ArrayList<Trip>();
		
		try {
			allViajes=Factories.persistence.getTripFinder().findAll();
			for (Trip trip : allViajes) {
				if(trip.disponible())
					viajes.add(trip);
			}
			
			Log.debug("Obtenida lista de viajes conteniendo [%d] viajes", viajes.size());
		}
		catch (Exception e) {
			Log.error("Algo ha ocurrido obteniendo lista de viajes");
		}
		return viajes;
	}
	
	@Override
	public String toString() {
		return getClass().getName();
	}
}
