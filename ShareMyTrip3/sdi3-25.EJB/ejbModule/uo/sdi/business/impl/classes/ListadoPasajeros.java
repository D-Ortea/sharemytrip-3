package uo.sdi.business.impl.classes;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import alb.util.log.Log;
import uo.sdi.business.Accion;
import uo.sdi.model.Application;
import uo.sdi.model.Seat;
import uo.sdi.model.Trip;
import uo.sdi.model.User;
import uo.sdi.model.types.SeatStatus;
import uo.sdi.persistencia.util.Jpa;

public class ListadoPasajeros implements Accion<Map<String, User[]>> {

	private Trip trip;

	public ListadoPasajeros(Trip trip){
		this.trip = trip;
	}
	
	@Override
	public Map<String, User[]> execute() {
		trip = Jpa.getManager().merge(trip);
		
		//Primero 
		List<User> appliedUsers = new ArrayList<>();
		List<User> seatUsers = new ArrayList<>();
		
		for (Seat s : trip.getSeats()) {
			if(s.getStatus().equals(SeatStatus.EXCLUDED)){
				appliedUsers.add(s.getUser());
				continue;
			} else {
				seatUsers.add(s.getUser());
			}
		}
		for (Application a : trip.getApplication()) {
			User u = a.getUser();
			if(!appliedUsers.contains(u))
				appliedUsers.add(a.getUser());
		}
		
		appliedUsers.removeAll(seatUsers);
		
		Map<String,User[]> result = new HashMap<String,User[]>();
		
		User[] seatUsersArray = new User[seatUsers.size()];
		User[] appliedUsersArray = new User[appliedUsers.size()];
		
		result.put("confirmados", (User[])seatUsers.toArray(seatUsersArray));
		result.put("noConfirmados", (User[])appliedUsers.toArray(appliedUsersArray));
		
		Log.info("Recuperado listad de pasajeros del viaje [%s] con [%s] usarios admitidos y [%s] usuarios no admitidos",
				trip.getId(),seatUsers.size(),appliedUsers.size());
		
		if(!trip.isNotOver())
			throw new RuntimeException();
		
		return result;
	}

}
