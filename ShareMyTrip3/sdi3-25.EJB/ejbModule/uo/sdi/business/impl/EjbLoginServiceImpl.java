package uo.sdi.business.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.jws.WebService;

import alb.util.log.Log;
import uo.sdi.infraestructura.Factories;
import uo.sdi.model.User;
import uo.sdi.model.types.UserStatus;
import uo.sdi.business.Accion;
import uo.sdi.business.AccionExecutor;
import uo.sdi.business.impl.local.LocalLoginService;
import uo.sdi.business.impl.remote.RemoteLoginService;

@Stateless
@WebService(name="LoginService")
public class EjbLoginServiceImpl implements RemoteLoginService, LocalLoginService{

	private AccionExecutor executor = new AccionExecutor();
	
	@Override
	public User verify(String login, String password) throws Exception {
		return executor.execute(new UserVerifier(login,password));
	}
	
	private class UserVerifier implements Accion<User>{
		
		private String password;
		private String login;
		
		public UserVerifier(String login, String password) {
			this.login = login;
			this.password = password;	
		}
		
		@Override
		public User execute() {
			List<User> userList = Factories.persistence.getUserFinder().findByLogin(login);
			User user = null;
			if(userList.size() > 1){
				Log.error("Hay varios usuarios con el login [%s]", login);
			}
			if(userList.size() == 0){
				return null;
			} else {
				user = userList.get(0);
				if(!user.getPassword().equals(password))
					return null;
			}
			
			if(user.getStatus().equals(UserStatus.CANCELLED)){
				Log.warn("Usuario [%s] esta deshabilitado y ha tratado de acceder"
						,user.getLogin());
				return null;
			}
				
			return user;
		}
		
	}
}
