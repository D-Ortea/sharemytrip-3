package uo.sdi.business;

public interface Accion<T> {
	public T execute() throws Exception;	
}
