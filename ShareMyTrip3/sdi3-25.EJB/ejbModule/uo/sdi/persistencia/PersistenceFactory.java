package uo.sdi.persistencia;

public interface PersistenceFactory {

	ApplicationFinder getApplicationFinder();
	TripFinder getTripFinder();
	SeatFinder getSeatFinder();
	UserFinder getUserFinder();
	RatingFinder getRatingFinder();
}
