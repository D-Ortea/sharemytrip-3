package uo.sdi.persistencia.classes;

import java.util.List;

import uo.sdi.model.Seat;
import uo.sdi.model.Trip;
import uo.sdi.model.User;
import uo.sdi.persistencia.SeatFinder;
import uo.sdi.persistencia.util.Jpa;

public class SeatFinderImpl implements SeatFinder{

	public List<Seat> findByKey(User user, Trip trip) {
		return Jpa.getManager().createNamedQuery("Seat.findByKey", Seat.class)
				.setParameter(1,user)
				.setParameter(2,trip)
				.getResultList();
	}
}
