package uo.sdi.persistencia.classes;

import uo.sdi.persistencia.ApplicationFinder;
import uo.sdi.persistencia.PersistenceFactory;
import uo.sdi.persistencia.RatingFinder;
import uo.sdi.persistencia.SeatFinder;
import uo.sdi.persistencia.TripFinder;
import uo.sdi.persistencia.UserFinder;

public class PersistenceFactoryImpl implements PersistenceFactory{

	@Override
	public ApplicationFinder getApplicationFinder() {
		return new ApplicationFinderImpl();
	}

	@Override
	public TripFinder getTripFinder() {
		return new TripFinderImpl();
	}

	@Override
	public SeatFinder getSeatFinder() {
		return new SeatFinderImpl();
	}

	@Override
	public UserFinder getUserFinder() {
		return new UserFinderImpl();
	}

	@Override
	public RatingFinder getRatingFinder() {
		return new RatingFinderImpl();
	}
	
	
}
