package uo.sdi.persistencia.classes;

import java.util.List;

import uo.sdi.model.Rating;
import uo.sdi.persistencia.RatingFinder;
import uo.sdi.persistencia.util.Jpa;

public class RatingFinderImpl implements RatingFinder {

	@Override
	public Rating findRating(Long id) {
		return Jpa.getManager().find(Rating.class, id);
	}

	@Override
	public List<Rating> findAll() {
		return Jpa.getManager()
				.createNamedQuery("Rating.findAll", Rating.class)
				.getResultList();
	}

}
