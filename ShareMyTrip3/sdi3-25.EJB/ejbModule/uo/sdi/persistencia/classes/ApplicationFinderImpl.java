package uo.sdi.persistencia.classes;

import java.util.List;

import uo.sdi.model.Application;
import uo.sdi.model.Trip;
import uo.sdi.model.User;
import uo.sdi.persistencia.ApplicationFinder;
import uo.sdi.persistencia.util.Jpa;

public class ApplicationFinderImpl implements ApplicationFinder{

	public List<Application> findByKey(User user, Trip trip) {
		return Jpa.getManager().createNamedQuery("Application.findByKey", Application.class)
				.setParameter(1,user)
				.setParameter(2,trip)
				.getResultList();
	}
}
