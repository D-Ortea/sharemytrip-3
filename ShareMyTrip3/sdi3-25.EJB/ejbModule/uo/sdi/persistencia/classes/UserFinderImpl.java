package uo.sdi.persistencia.classes;

import java.util.List;

import uo.sdi.model.User;
import uo.sdi.persistencia.UserFinder;
import uo.sdi.persistencia.util.Jpa;

public class UserFinderImpl implements UserFinder{

	public List<User> findByLogin(String login) {
		return Jpa.getManager()
				.createNamedQuery("User.findByLogin", User.class)
				.setParameter(1, login)
				.getResultList();
	}
	
	public List<User> fetchPromoted(Long id) {
		return Jpa.getManager()
				.createNamedQuery("User.fetchPromoted", User.class)
				.setParameter(1, id)
				.getResultList();
	}
	
	public User findByID(Long ID) {
		return Jpa.getManager().find(User.class, ID);
	}

	@Override
	public List<User> findAll() {
		return Jpa.getManager()
				.createNamedQuery("User.findAll", User.class)
				.getResultList();
	}

	@Override
	public List<User> findActivos() {
		return Jpa.getManager()
				.createNamedQuery("User.findActivos", User.class)
				.getResultList();
	}
}
