package uo.sdi.persistencia.classes;

import java.util.List;

import uo.sdi.model.Trip;
import uo.sdi.persistencia.TripFinder;
import uo.sdi.persistencia.util.Jpa;

public class TripFinderImpl implements TripFinder{

	public List<Trip> findAll() {
		return Jpa.getManager().createNamedQuery("Trip.findAll", Trip.class)
				.getResultList();
	}

	public Trip findByID(Long ID) {
		return Jpa.getManager().find(Trip.class, ID);
	}

}
