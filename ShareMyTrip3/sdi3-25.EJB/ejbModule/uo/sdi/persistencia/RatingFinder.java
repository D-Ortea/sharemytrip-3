package uo.sdi.persistencia;

import java.util.List;

import uo.sdi.model.Rating;

public interface RatingFinder {

	Rating findRating(Long id);
	List<Rating> findAll();
}
