package uo.sdi.persistencia;

import java.util.List;

import uo.sdi.model.Application;
import uo.sdi.model.Trip;
import uo.sdi.model.User;

public interface ApplicationFinder {
	
	public List<Application> findByKey(User user, Trip trip);
}
