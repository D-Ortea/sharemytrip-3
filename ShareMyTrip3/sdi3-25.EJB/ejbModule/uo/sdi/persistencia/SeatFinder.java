package uo.sdi.persistencia;

import java.util.List;

import uo.sdi.model.Seat;
import uo.sdi.model.Trip;
import uo.sdi.model.User;

public interface SeatFinder {

	public List<Seat> findByKey(User user, Trip trip);
}
