package uo.sdi.persistencia;

import java.util.List;

import uo.sdi.model.User;

public interface UserFinder {
	
	public List<User> findByLogin(String login);
	
	public List<User> fetchPromoted(Long id);
	
	public User findByID(Long ID);

	public List<User> findAll();

	public List<User> findActivos();
}
