package uo.sdi.persistencia;

import java.util.List;

import uo.sdi.model.Trip;

public interface TripFinder {

	public List<Trip> findAll();

	public Trip findByID(Long ID);
}
